{-# LANGUAGE TemplateHaskell #-}
{-#  LANGUAGE FlexibleContexts #-}
module Utils (
 noteV
,noteVIO
) where


import qualified Data.DList as DL
import Control.Lens hiding ((|>),(<.>))
import qualified Control.Lens as L --hiding (element)
import Control.Monad.State.Lazy
import qualified Control.Monad.State.Lazy as St
import Control.Monad.RWS.Lazy
import qualified Control.Monad.RWS.Lazy as Rws


import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr
                         ,replicate)

import Types


L.makeLenses ''HticConfig





noteV :: (Show b, MonadReader HticConfig m
          ,MonadWriter (DList ApplicationLog) m) => String -> b -> m ()
noteV v x = do 
   a<-ask
   when (elem v $ a^.hticConf_verbosity) 
                                   $ tell $ DL.singleton $ Note $ show x



noteVIO :: String -> HticConfig -> String -> IO ()
noteVIO v a x = do 
   when (elem v $ a^.hticConf_verbosity) $ putStrLn x



