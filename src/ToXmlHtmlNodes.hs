{-# LANGUAGE TemplateHaskell #-}
-- {-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DeriveGeneric #-}

-- {-#  LANGUAGE FlexibleInstances #-}
-- {-#  LANGUAGE FlexibleContexts #-}
-- {-#  LANGUAGE TypeSynonymInstances #-}


module ToXmlHtmlNodes (
 nodesReplaceChildrenInNodeById
,nodesReplaceDisplay
,nodesReplaceData
,documentReplaceDisplay
,documentReplaceData
,displayableNodeFromBranch
,nodeFromBranch
) where

import qualified Data.ByteString.Lazy as B
import qualified Data.Binary.Builder as BB
import Data.Maybe
import Data.Bool
import qualified Data.Map as DM
import qualified Data.Text as T
import Control.Lens hiding ((|>),(<.>))
import qualified Control.Lens as L --hiding (element)
import Control.Monad.State.Lazy
import qualified Control.Monad.State.Lazy as St
import Control.Monad.RWS.Lazy
import qualified Control.Monad.RWS.Lazy as Rws
import GHC.Generics (Generic, Generic1)

import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr
                         ,replicate)
import Text.XmlHtml


import Types



L.makeLenses ''Branch
L.makeLenses ''BNode
L.makeLenses ''TNode
L.makeLenses ''Connection
L.makeLenses ''DisplayOnNewPageNode
L.makeLenses ''Point
L.makeLenses ''TextFillIn
L.makeLenses ''Goto
L.makeLenses ''MeaningfullNodes
L.makeLenses ''HticConfig










nodeFromBranch :: Branch -> Node 
nodeFromBranch b =
  dataWithValue (b^.b_name) $ [
    dataWithValue "option"  $ nodesFromText (b^.b_name) $ b^.b_option
   ,dataWithValue "content" $ nodesFromText (b^.b_name) $ b^.b_content 
   ,dataWithValue "next" $ map (\v-> paramWithValue v []) $ b^.b_next
   ] ++ (bool [] [paramWithValue "displayOnNewPage" []] 
                 $ b^.b_displayOnNewPage
        ) 
     ++ (maybe [] 
               (\c->[dataWithValue "altContent" 
                                   $ nodesFromText (b^.b_name) c
                    ]
               )
               $ b^.b_altContent
        ) 
     ++ (maybe [] 
               (\c->[dataWithValue "altContentTimeout" 
                                   $ nodesFromText (b^.b_name) 
                                   $ T.pack $ show c
                    ]
               )
               $ b^.b_altContentTimeout
        ) 
     ++ (bool [] [paramWithValue "randomNext" []] 
                 $ b^.b_randomNext
        ) 
     ++ (maybe [] 
               (\c->[dataWithValue "autoContinue" 
                                   $ nodesFromText (b^.b_name) 
                                   $ T.pack $ show c
                    ]
               )
               $ b^.b_autoContinue
        )
     ++ (maybe [] 
               (\c->[dataWithValue "overrideNext" 
                                   $ nodesFromText (b^.b_name) c
                    ]
               )
               $ b^.b_overrideNext
        )  
     ++ (buildFromTemplateIfPresent 
          $ mapMaybe buildFromTemplatePerEntry 
                     $ b^.b_buildFromTamplate
        )  
     ++ (maybe [] 
               (\c->[dataWithValue "skipPastNext" 
                                   $ nodesFromText (b^.b_name) c
                    ]
               )
               $ b^.b_skipPastNext
        )  
     ++ (maybe [] 
               (\c->[dataWithValue "runCode" 
                                   $ nodesFromText (b^.b_name) c
                    ]
               )
               $ b^.b_runCode
        ) 
     ++ (maybe [] 
               (\c->[dataWithValue "optionChosenState" 
                                   $ nodesFromText (b^.b_name) c
                    ]
               )
               $ b^.b_optionChosenState
        ) 
  where
    buildFromTemplateIfPresent [] = []
    buildFromTemplateIfPresent x = [dataWithValue "buildFromTemplate" x]
    
    buildFromTemplatePerEntry (TplData t) = Just 
                  $ dataWithValue "data" $ nodesFromText (b^.b_name) t
    buildFromTemplatePerEntry (TplCache t) = Just 
                  $ dataWithValue "cache" $ nodesFromText (b^.b_name) t
    buildFromTemplatePerEntry _ = Nothing
        
nodesFromText error x = either 
 (\e->[TextNode $ T.concat [T.pack e,x]]) 
 (\(HtmlDocument {docContent =dc})-> dc)
 $ parseHTML (T.unpack error) 
 $ B.toStrict $ BB.toLazyByteString $ BB.putStringUtf8 $ T.unpack x

 
 
 
displayableNodeFromBranch :: DM.Map T.Text Branch -> Branch -> [Node] 
displayableNodeFromBranch mb b = 
  (:) (dataWithValue (T.concat ["content_", b^.b_name]) 
        $ nodesFromText (b^.b_name) $ b^.b_content
      )  

      $ mapMaybe (\optionId-> maybe Nothing 
            (\optBranch->Just 
              $ dataWithValue (T.concat ["option_", optionId]) 
              $ nodesFromText (optBranch^.b_name) $ optBranch^.b_option
            )
            $ DM.lookup optionId mb
          ) 
          $ b^.b_next
     

 
  
elemWithValue t v c = Element {
   elementTag = t
  ,elementAttrs = [("value",v)]
  ,elementChildren = c
  }

dataWithValue   v c = elemWithValue "data"   v c
paramWithValue v c = elemWithValue "param" v c



documentReplaceData :: Document -> [Node] -> Document
documentReplaceData 
  d@(HtmlDocument 
     {docContent =dc}) 
  replacement
     = (\doc-> doc {docContent=nodesReplaceData dc replacement}) d
   

documentReplaceData x _ = x


documentReplaceDisplay :: Document -> [Node] -> Document
documentReplaceDisplay 
  d@(HtmlDocument 
     {docContent =dc}) 
  replacement
     = (\doc-> doc {docContent=nodesReplaceDisplay dc replacement}) d
   

documentReplaceDisplay x _ = x



nodesReplaceData :: [Node] -> [Node] -> [Node]
nodesReplaceData n replacement =
  nodesReplaceChildrenInNodeById "data" n replacement


nodesReplaceDisplay :: [Node] -> [Node] -> [Node]
nodesReplaceDisplay n replacement =
  nodesReplaceChildrenInNodeById "display" n replacement


nodesReplaceChildrenInNodeById :: T.Text -> [Node] -> [Node] -> [Node]
nodesReplaceChildrenInNodeById id_ n replacement =
  --map (inTag "html" (map (inTag "body" (map (inId id_ (\_-> replacement)))))) n
  map (inId' id_) n
  where
  inTag tag run e@(Element {elementChildren=ec})
    |(tagName e)==(Just tag) = (\el-> el {elementChildren=run ec}) e
    |otherwise = e
    
  inTag _ _ e = e

  inId id_ run e@(Element {elementChildren=ec})
    |(getAttribute "id" e) == (Just id_) = 
                                   (\el-> el {elementChildren=run ec}) e
    |otherwise = e
    
  inId _ _ e = e

  inId' id_ e@(Element {elementChildren=ec})
    |(getAttribute "id" e) == (Just id_) = 
                              (\el-> el {elementChildren=replacement}) e
    |otherwise = (\el-> el {elementChildren=map (inId' id_) ec}) e
    
  inId' _ e = e






















