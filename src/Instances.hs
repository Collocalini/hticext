{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

{-#  LANGUAGE FlexibleInstances #-}
{-#  LANGUAGE FlexibleContexts #-}
{-#  LANGUAGE TypeSynonymInstances #-}


module Instances (
 unwrapMeaningfullBnode
,getContent
,putContent
,getConnections
,putConnection
,getTextRelated
,getTextRelatedMa
,putTextRelated
,ifDisplayOnNewPage
,setDisplayOnNewPage
,applyTemplate
,toMeaningfull
,fromMeaningfull
,evalNodeStructure
,selectivePut
,selectiveGet
,setAltContent
,setAltContent'
,setRunCode
,setRunCode'
,setChosen
,setChosen'
,getId
,getAnyMeaningfullNode
,consumesConnected
,getOverrideNext
,setOverrideNext
,refId
,tNodeReference
,toMeaningfullReference
) where


import Data.Maybe
import Data.List
import qualified Data.DList as DL
import qualified Data.Map as DM
import qualified Data.Text as T
import Control.Lens hiding ((|>),(<.>),chosen)
import qualified Control.Lens as L --hiding (element)
import Control.Monad.State.Lazy
import qualified Control.Monad.State.Lazy as St
import Control.Monad.RWS.Lazy
import qualified Control.Monad.RWS.Lazy as Rws
import GHC.Generics (Generic, Generic1)

import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr
                         ,replicate)

import Types
import Classes
import Utils

L.makeLenses ''Branch
L.makeLenses ''BNode
L.makeLenses ''TNode
L.makeLenses ''Connection
L.makeLenses ''DisplayOnNewPageNode
L.makeLenses ''Point
L.makeLenses ''TextFillIn
L.makeLenses ''Goto
L.makeLenses ''MeaningfullNodes
L.makeLenses ''HticConfig
L.makeLenses ''AltContent
L.makeLenses ''RandomNext
L.makeLenses ''AutoContinue
L.makeLenses ''OverrideNext
L.makeLenses ''RunCode
L.makeLenses ''Chosen

instance Eq ConnectionOverSomething where 
  (ConnectionOverGoto (GR gl) (BNR bl)) 
    == (ConnectionOverGoto (GR gr) (BNR br))     = gl == gr && bl == br
  (DirectConnection (BNR bl)) == (DirectConnection (BNR br)) = bl == br
  (HangingConnection (GR gl)) == (HangingConnection (GR gr)) = gl == gr
  
  _ == _ = False
  
instance Ord ConnectionOverSomething where
   compare (DirectConnection _) _ = LT
   compare _ (DirectConnection _) = GT
   
   compare (DirectConnection (BNR bl)) 
           (DirectConnection (BNR br)) = compare bl br
           
   compare (HangingConnection _) _ = GT
   compare _ (HangingConnection _) = LT
   
   compare (HangingConnection (GR gl))
           (HangingConnection (GR gr)) = compare gl gr
   
   compare (ConnectionOverGoto (GR gl) (BNR bl)) 
           (ConnectionOverGoto (GR gr) (BNR br))
     |bl/=br    = compare bl br
     |otherwise = compare gl gr






instance Eq Point where 
  p1 == p2 = ((p1^.pt_x) == (p2^.pt_x)) && ((p1^.pt_y) == (p2^.pt_y))

instance Ord Point where
   compare p1 p2
     |(p1^.pt_y)/=(p2^.pt_y) = compare (p1^.pt_y) (p2^.pt_y)
     |otherwise              = compare (p1^.pt_x) (p2^.pt_x)
   p1 < p2
     |(p1^.pt_y)/=(p2^.pt_y) = (p1^.pt_y) < (p2^.pt_y)
     |otherwise              = (p1^.pt_x) < (p2^.pt_x)
   p1 > p2
     |(p1^.pt_y)/=(p2^.pt_y) = (p1^.pt_y) > (p2^.pt_y)
     |otherwise              = (p1^.pt_x) > (p2^.pt_x)
   p1 <= p2
     |(p1^.pt_y)/=(p2^.pt_y) = (p1^.pt_y) <= (p2^.pt_y)
     |otherwise              = (p1^.pt_x) <= (p2^.pt_x)
   p1 >= p2
     |(p1^.pt_y)/=(p2^.pt_y) = (p1^.pt_y) >= (p2^.pt_y)
     |otherwise              = (p1^.pt_x) >= (p2^.pt_x)
   max p1 p2
     |(p1^.pt_y)/=(p2^.pt_y) = byY
     |otherwise              = byX
     where 
     byY 
       |(p1^.pt_y)<(p2^.pt_y) = p2
       |otherwise             = p1
     byX 
       |(p1^.pt_x)<(p2^.pt_x) = p2
       |otherwise             = p1
   min p1 p2
     |(p1^.pt_y)/=(p2^.pt_y) = byY
     |otherwise              = byX
     where 
     byY 
       |(p1^.pt_y)<(p2^.pt_y) = p1
       |otherwise             = p2
     byX 
       |(p1^.pt_x)<(p2^.pt_x) = p1
       |otherwise             = p2
   






instance Eq Goto where 
  g1 == g2 = ((g1^.g_option)        == (g2^.g_option)) 
               && ((g1^.g_related)  == (g2^.g_related)) 
               && ((g1^.g_id)       == (g2^.g_id)) 
               && ((g1^.g_mark)     == (g2^.g_mark)) 
               && ((g1^.g_position) == (g2^.g_position)) 

instance Eq TextFillIn where 
  t1 == t2 = ((t1^.tx_content)       == (t2^.tx_content)) 
               && ((t1^.tx_name)     == (t2^.tx_name)) 
               && ((t1^.tx_id)       == (t2^.tx_id)) 
               && ((t1^.tx_link)     == (t2^.tx_link)) 
               && ((t1^.tx_position) == (t2^.tx_position)) 


instance Ord Goto where
   compare g1 g2
     |(g1^.g_position)/=(g2^.g_position) = compare (g1^.g_position) 
                                                   (g2^.g_position)
     |otherwise              = compare (g1^.g_id) (g2^.g_id)

   g1 < g2
     |(g1^.g_position)/=(g2^.g_position) = (g1^.g_position)<(g2^.g_position)
     |otherwise              = (g1^.g_id) < (g2^.g_id)
   g1 > g2
     |(g1^.g_position)/=(g2^.g_position)  = (g1^.g_position)>(g2^.g_position)
     |otherwise              = (g1^.g_id) > (g2^.g_id)
   g1 <= g2
     |(g1^.g_position)/=(g2^.g_position) = (g1^.g_position)<=(g2^.g_position)
     |otherwise              = (g1^.g_id) <= (g2^.g_id)
   g1 >= g2
     |(g1^.g_position)/=(g2^.g_position)  = (g1^.g_position)>=(g2^.g_position)
     |otherwise              = (g1^.g_id) >= (g2^.g_id)
   max g1 g2
     |(g1^.g_position)/=(g2^.g_position) = byPos
     |otherwise                          = byId
     where
     byPos 
       |(g1^.g_position)<(g2^.g_position) = g2
       |otherwise                         = g1
     byId
       |(g1^.g_id)<(g2^.g_id) = g2
       |otherwise             = g1
   min g1 g2
     |(g1^.g_position)/=(g2^.g_position) = byPos
     |otherwise                          = byId
     where
     byPos 
       |(g1^.g_position)<(g2^.g_position) = g1
       |otherwise                         = g2
     byId
       |(g1^.g_id)<(g2^.g_id) = g1
       |otherwise             = g2



instance Monoid ApplicationLog where
   mempty = ApplicationLogEmpty
   mconcat xs = Dump $ unlines $ map show xs



instance AwareOfConnected (NodeStructure BNode) where
  consumesConnected (ToA m@(IsTextFillIn _)) n = putTextRelated m n
  consumesConnected (ToA m@(IsGoto       _)) n = addConnection m n
                                                               
  consumesConnected (ToA m@(IsBNode      _)) n = addConnection m n
                                                               
  consumesConnected (ToA m@(IsDisplayOnNewPageNode _)) n = 
                                                 setDisplayOnNewPage m n
  
  consumesConnected (ToA m@(IsRandomNext _)) n = setRandomNext m n
                                                                             
  consumesConnected (ToA m@(IsAltContentNode _)) n = setAltContent m n
  
  consumesConnected (ToA m@(IsRunCodeNode _)) n = setRunCode m n
  
  consumesConnected (ToA m@(IsAutoContinue _)) n = setAutoContinue m n
  
  consumesConnected (ToA m@(IsOverrideNext _)) n = setOverrideNext m n
  
  consumesConnected (ToA m@(IsTNode _)) n = setTNode m n
  
  consumesConnected (FromA m@(IsTextFillIn _)) n = putTextRelated m n
  
  consumesConnected (FromA m@(IsGoto       _)) n = addConnection m n
                                                               
  consumesConnected (FromA m@(IsDisplayOnNewPageNode _)) n = 
                                                 setDisplayOnNewPage m n
                                                 
  consumesConnected (FromA m@(IsRandomNext _)) n = setRandomNext m n                                                 
                                                 
  consumesConnected (FromA m@(IsAltContentNode _)) n = setAltContent m n
  
  consumesConnected (FromA m@(IsRunCodeNode _)) n = setRunCode m n
  
  consumesConnected (FromA m@(IsAutoContinue _)) n = setAutoContinue m n
  
  consumesConnected (FromA m@(IsOverrideNext _)) n = setOverrideNext m n
  
  consumesConnected (FromA m@(IsTNode _)) n = setTNode m n
  
  consumesConnected _ n = n
  

instance AwareOfConnected (NodeStructure TNode) where
  
  consumesConnected (ToA   m@(IsGoto _)) n = addConnection m n
  consumesConnected (FromA m@(IsGoto _)) n = addConnection m n
  consumesConnected _ n = n


instance AwareOfConnected (NodeStructure Goto) where
  consumesConnected (ToA m@(IsTextFillIn _)) n = putTextRelated m n
  consumesConnected (ToA m@(IsChosenNode _)) n = setChosen m n
  
  consumesConnected (FromA m@(IsTextFillIn _)) n = putTextRelated m n
  consumesConnected (FromA m@(IsChosenNode _)) n = setChosen m n
  
  consumesConnected _ n = n
  
{-
consumesConnectedPutConnectionOverMeaningfull m n = do
    n'<-n
    (return.fromJust.fromMeaningfull) 
      =<< (putConnection m Nothing . return . toMeaningfull)
      =<< n
-}

instance AwareOfConnected (NodeStructure AltContent) where
  consumesConnected (ToA m@(IsTextFillIn _)) n = putTextRelated m n
  --consumesConnected (ToA m@(IsBNode      _)) n = 
  --                     consumesConnectedPutConnectionOverMeaningfull m n   
                                                               
                                                               

  consumesConnected (FromA m@(IsTextFillIn _)) n = putTextRelated m n
  --consumesConnected (ToA m@(IsBNode      _)) n = 
  --                     consumesConnectedPutConnectionOverMeaningfull m n  
                            
  consumesConnected _ n = n



instance AwareOfConnected (NodeStructure Chosen) where
  consumesConnected (ToA m@(IsTextFillIn _)) n = putTextRelated m n
  consumesConnected (FromA m@(IsTextFillIn _)) n = putTextRelated m n
  consumesConnected _ n = n





instance AwareOfConnected (NodeStructure Meaningfull) where
  consumesConnected d n = do 
    n'<-n
    let cc x = do 
                n''<-consumesConnected d $ return x
                return $ toMeaningfull n''
    case n' of
      IsBNode      x -> cc x
      IsTNode      x -> cc x
      IsGoto       x -> cc x
      IsAltContentNode x -> cc x
      IsChosenNode     x -> cc x
      _         -> n
    

    
  
instance ReferenceToMeaningfull BNodeReference where
  refId (BNR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsBNodeReference r
  fromMeaningfullReference (IsBNodeReference r) = Just r
  fromMeaningfullReference _ = Nothing
  
instance ReferenceToMeaningfull TNodeReference where
  refId (TNR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsTNodeReference r
  fromMeaningfullReference (IsTNodeReference r) = Just r
  fromMeaningfullReference _ = Nothing

instance ReferenceToMeaningfull GotoReference where
  refId (GR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsGotoReference r
  fromMeaningfullReference (IsGotoReference r) = Just r
  fromMeaningfullReference _ = Nothing

instance ReferenceToMeaningfull TextFillInReference where
  refId (TxR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsTextFillInReference r
  fromMeaningfullReference (IsTextFillInReference r) = Just r
  fromMeaningfullReference _ = Nothing

instance ReferenceToMeaningfull AltContentReference where
  refId (ACR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsAltContentNodeReference r
  fromMeaningfullReference (IsAltContentNodeReference r) = Just r
  fromMeaningfullReference _ = Nothing

instance ReferenceToMeaningfull RunCodeNodeReference where
  refId (RNCR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsRunCodeReference r
  fromMeaningfullReference (IsRunCodeReference r) = Just r
  fromMeaningfullReference _ = Nothing

instance ReferenceToMeaningfull AutoContinueReference where
  refId (AUCR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsAutoContinueReference r
  fromMeaningfullReference (IsAutoContinueReference r) = Just r
  fromMeaningfullReference _ = Nothing

instance ReferenceToMeaningfull DisplayOnNewPageNodeReference where
  refId (DONPR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsDisplayOnNewPageNodeReference r
  fromMeaningfullReference (IsDisplayOnNewPageNodeReference r) = Just r
  fromMeaningfullReference _ = Nothing


instance ReferenceToMeaningfull RandomNextReference where
  refId (RNR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsRandomNextReference r
  fromMeaningfullReference (IsRandomNextReference r) = Just r
  fromMeaningfullReference _ = Nothing


instance ReferenceToMeaningfull OverrideNextReference where
  refId (OVNR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsOverrideNextReference r
  fromMeaningfullReference (IsOverrideNextReference r) = Just r
  fromMeaningfullReference _ = Nothing

  
instance ReferenceToMeaningfull BNode where
  refId n = getId n
  toMeaningfullReference n = IsBNodeReference $ BNR $ getId n 
  fromMeaningfullReference _ = Nothing
  
instance ReferenceToMeaningfull TNode where
  refId n = getId n
  toMeaningfullReference n = IsTNodeReference $ TNR $ getId n 
  fromMeaningfullReference _ = Nothing
  

instance ReferenceToMeaningfull ChosenNodeReference where
  refId (CsnR id_) = id_
  refId _ = ""
  toMeaningfullReference r = IsChosenReference r
  fromMeaningfullReference (IsChosenReference r) = Just r
  fromMeaningfullReference _ = Nothing




instance MeaningfullNodesContainer MeaningfullNodes where
  selectivePut (IsBNode n) m = 
        meaningfullNodesContainer_putCommon n m (bnodes) (bnodes)
  selectivePut (IsTNode n) m = 
        meaningfullNodesContainer_putCommon n m (tnodes) (tnodes)     
  selectivePut (IsGoto n) m = 
        meaningfullNodesContainer_putCommon n m (goto) (goto) 
  selectivePut (IsTextFillIn n) m = 
        meaningfullNodesContainer_putCommon n m (textFillIn) (textFillIn) 
  selectivePut (IsDisplayOnNewPageNode n) m = 
        meaningfullNodesContainer_putCommon n m (propDisplayOnNewPage) 
                                               (propDisplayOnNewPage) 
  selectivePut (IsAltContentNode n) m = 
        meaningfullNodesContainer_putCommon n m (altContent) (altContent)                
  
  selectivePut (IsRunCodeNode n) m = 
        meaningfullNodesContainer_putCommon n m (runCode) (runCode)   
  
  selectivePut (IsAutoContinue n) m = 
        meaningfullNodesContainer_putCommon n m (autoContinue) (autoContinue) 
  
  selectivePut (IsRandomNext n) m = 
        meaningfullNodesContainer_putCommon n m (randomNext) (randomNext)                
  
  selectivePut (IsOverrideNext n) m = 
        meaningfullNodesContainer_putCommon n m (overrideNext) (overrideNext)                

  selectivePut (IsChosenNode n) m = 
        meaningfullNodesContainer_putCommon n m (chosen) (chosen) 


  selectivePut _ m = m

  selectiveGet (IsBNodeReference (BNR r)) m = 
          meaningfullNodesContainer_getCommon r m (bnodes)
  selectiveGet (IsTNodeReference (TNR r)) m = 
          meaningfullNodesContainer_getCommon r m (tnodes)       
  selectiveGet (IsTextFillInReference (TxR r)) m = 
          meaningfullNodesContainer_getCommon r m (textFillIn)
  selectiveGet (IsGotoReference (GR r)) m = 
          meaningfullNodesContainer_getCommon r m (goto)
  selectiveGet (IsDisplayOnNewPageNodeReference (DONPR r)) m = 
          meaningfullNodesContainer_getCommon r m (propDisplayOnNewPage)
  selectiveGet (IsAltContentNodeReference (ACR r)) m = 
          meaningfullNodesContainer_getCommon r m (altContent)
  selectiveGet (IsRunCodeReference (RNCR r)) m = 
          meaningfullNodesContainer_getCommon r m (runCode)
  selectiveGet (IsAutoContinueReference (AUCR r)) m = 
          meaningfullNodesContainer_getCommon r m (autoContinue)        
  selectiveGet (IsRandomNextReference (RNR r)) m = 
          meaningfullNodesContainer_getCommon r m (randomNext)
  selectiveGet (IsOverrideNextReference (OVNR r)) m = 
          meaningfullNodesContainer_getCommon r m (overrideNext)       
  selectiveGet (IsChosenReference (CsnR r)) m = 
          meaningfullNodesContainer_getCommon r m (chosen)
          
  selectiveGet _ _ = []

  getAnyMeaningfullNode id m = concat [
      selectiveGet (toMeaningfullReference $ BNR id) m
     ,selectiveGet (toMeaningfullReference $ TNR id) m
     ,selectiveGet (toMeaningfullReference $ GR  id) m
     ,selectiveGet (toMeaningfullReference $ TxR id) m
     ,selectiveGet (toMeaningfullReference $ DONPR id) m
     ,selectiveGet (toMeaningfullReference $ ACR id) m
     ,selectiveGet (toMeaningfullReference $ AUCR id) m
     ,selectiveGet (toMeaningfullReference $ RNR id) m
     ,selectiveGet (toMeaningfullReference $ OVNR id) m
     ,selectiveGet (toMeaningfullReference $ RNCR id) m
     ,selectiveGet (toMeaningfullReference $ CsnR id) m
     ]


meaningfullNodesContainer_getCommon id m getter = map toMeaningfull
                       $ fromMaybe []
                       $ DM.lookup id $ m^.getter

meaningfullNodesContainer_putCommon n m setter getter = set setter 
                (DM.insertWith (++)
                               (getId n) 
                               [n] 
                               (m^.getter)
                )
                m
    
    
  --selectiveGet :: (ReferenceToMeaningfull r,OneOfMeaningfull n) 
  --             => r -> m a -> m n



instance OneOfMeaningfull BNode where
  toMeaningfull n = IsBNode n
  fromMeaningfull (IsBNode n) = Just n
  fromMeaningfull _ = Nothing
  
instance OneOfMeaningfull TNode where
  toMeaningfull n = IsTNode n
  fromMeaningfull (IsTNode n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull Goto where
  toMeaningfull n = IsGoto n
  fromMeaningfull (IsGoto n) = Just n
  fromMeaningfull _ = Nothing


instance OneOfMeaningfull AltContent where
  toMeaningfull n = IsAltContentNode n
  fromMeaningfull (IsAltContentNode n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull RunCode where
  toMeaningfull n = IsRunCodeNode n
  fromMeaningfull (IsRunCodeNode n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull AutoContinue where
  toMeaningfull n = IsAutoContinue n
  fromMeaningfull (IsAutoContinue n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull TextFillIn where
  toMeaningfull n = IsTextFillIn n
  fromMeaningfull (IsTextFillIn n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull DisplayOnNewPageNode where
  toMeaningfull n = IsDisplayOnNewPageNode n
  fromMeaningfull (IsDisplayOnNewPageNode n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull RandomNext where
  toMeaningfull n = IsRandomNext n
  fromMeaningfull (IsRandomNext n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull OverrideNext where
  toMeaningfull n = IsOverrideNext n
  fromMeaningfull (IsOverrideNext n) = Just n
  fromMeaningfull _ = Nothing
  
instance OneOfMeaningfull Chosen where
  toMeaningfull n = IsChosenNode n
  fromMeaningfull (IsChosenNode n) = Just n
  fromMeaningfull _ = Nothing

instance OneOfMeaningfull Meaningfull where
  toMeaningfull n = n
  fromMeaningfull n = Just n


instance HasId BNode where
  getId n = n^.bn_name
  
instance HasId TNode where
  getId n = n^.tn_id

instance HasId Goto where
  getId n = n^.g_id

instance HasId TextFillIn where
  getId n = n^.tx_id

instance HasId DisplayOnNewPageNode where
  getId n = n^.donp_id

instance HasId AltContent where
  getId n = n^.ac_id

instance HasId RunCode where
  getId n = n^.rnc_id

instance HasId AutoContinue where
  getId n = n^.auc_id

instance HasId RandomNext where
  getId n = n^.rndnxt_id

instance HasId OverrideNext where
  getId n = n^.ovn_id 
  
instance HasId Chosen where
  getId n = n^.csn_id


instance ContainingTextFillins NodeStructure where
  textByReference id = do 
    use_textFillIn <- use textFillIn
    id'<-id
    return $ listToMaybe 
           $ concat 
           $ maybeToList 
           $ DM.lookup id' use_textFillIn


instance ContainingTextFillinReferences BNode where
  textReferences n = n^.bn_textRelated
  consTextReference t n = set bn_textRelated (t:(n^.bn_textRelated)) n
  
instance ContainingTextFillinReferences Goto where
  textReferences n = n^.g_related
  consTextReference t n = set g_related (t:(n^.g_related)) n
  
instance ContainingTextFillinReferences AltContent where
  textReferences n = n^.ac_textRelated
  consTextReference t n = set ac_textRelated (t:(n^.ac_textRelated)) n
  
instance ContainingTextFillinReferences TextFillIn where
  textReferences n = []
  consTextReference _ n = n
  
instance ContainingTextFillinReferences DisplayOnNewPageNode where
  textReferences _ = []
  consTextReference _ n = n
  
instance ContainingTextFillinReferences RandomNext where
  textReferences _ = []
  consTextReference _ n = n

instance ContainingTextFillinReferences OverrideNext where
  textReferences _ = []
  consTextReference _ n = n
      
instance ContainingTextFillinReferences Chosen where
  textReferences n = n^.csn_related
  consTextReference t n = set csn_related (t:(n^.csn_related)) n


instance ContainingAltContentReference BNode where
  altContentReference n = n^.bn_altContent
  setAltContentReference a n = set bn_altContent (Just a) n

instance ContainingRunCodeReference BNode where
  runCodeReference n = n^.bn_runCode
  setRunCodeReference a n = set bn_runCode (Just a) n

 
instance ContainingAltContentReference Meaningfull where
  altContentReference (IsBNode n) = altContentReference n
  altContentReference _ = Nothing
  setAltContentReference a (IsBNode n) = toMeaningfull 
                                          $ setAltContentReference a n
  setAltContentReference _ n = n

instance ContainingRunCodeReference Meaningfull where
  runCodeReference (IsBNode n) = runCodeReference n
  runCodeReference _ = Nothing
  setRunCodeReference a (IsBNode n) = toMeaningfull 
                                          $ setRunCodeReference a n
  setRunCodeReference _ n = n
 
instance ContainingAutoContinueReference BNode where
  autoContinueReference n = n^.bn_autoContinue
  setAutoContinueReference a n = set bn_autoContinue (Just a) n
 
instance ContainingAutoContinueReference Meaningfull where
  autoContinueReference (IsBNode n) = autoContinueReference n
  autoContinueReference _ = Nothing
  setAutoContinueReference a (IsBNode n) = toMeaningfull 
                                          $ setAutoContinueReference a n
  setAutoContinueReference _ n = n

instance ContainingTNodeReference BNode where
  tNodeReference n = n^.bn_template
  setTNodeReference tn n = set bn_template (Just tn) n   
                  
instance ContainingChosenReference Goto where
  chosenReference n = n^.g_chosen
  setChosenReference oc n = set g_chosen (Just oc) n

instance ContainingTNodeReference Meaningfull where
  tNodeReference (IsBNode n) = tNodeReference n
  tNodeReference _ = Nothing
  setTNodeReference tn (IsBNode n) = toMeaningfull 
                                          $ setTNodeReference tn n
  setTNodeReference _ n = n
  
 
instance PosessingAltContentMa NodeStructure where
  getAltContent n = do 
    mn<-get
    n'<-n
    return 
      $ maybe []
      (\r-> mapMaybe fromMeaningfull 
                     $ selectiveGet (toMeaningfullReference r) mn)
      $ altContentReference n'
      
 
  setAltContent m@(IsAltContentNode a) n = do
    mn<-get
    n'<-n
    put $ selectivePut m mn
    return $ setAltContentReference (ACR $ getId a) n'
  
  setAltContent _ n = n

  setAltContent' a n = setAltContent (IsAltContentNode a) n

instance PosessingRunCodeMa NodeStructure where
  getRunCode n = do 
    mn<-get
    n'<-n
    return 
      $ maybe []
      (\r-> mapMaybe fromMeaningfull 
                     $ selectiveGet (toMeaningfullReference r) mn)
      $ runCodeReference n'
      
 
  setRunCode m@(IsRunCodeNode a) n = do
    mn<-get
    n'<-n
    put $ selectivePut m mn
    return $ setRunCodeReference (RNCR $ getId a) n'
  
  setRunCode _ n = n

  setRunCode' a n = setRunCode (IsRunCodeNode a) n


instance PosessingAutoContinueMa NodeStructure where
  getAutoContinue n = do 
    mn<-get
    n'<-n
    return 
      $ maybe []
      (\r-> mapMaybe fromMeaningfull 
                     $ selectiveGet (toMeaningfullReference r) mn)
      $ autoContinueReference n'
      
 
  setAutoContinue m@(IsAutoContinue a) n = do
    mn<-get
    n'<-n
    put $ selectivePut m mn
    return $ setAutoContinueReference (AUCR $ getId a) n'
  
  setAutoContinue _ n = n

  setAutoContinue' a n = setAutoContinue (IsAutoContinue a) n

instance PosessingTNodeMa NodeStructure where
  getTNode n = do 
    mn<-get
    n'<-n
    return 
      $ maybe []
      (\r-> mapMaybe fromMeaningfull 
                     $ selectiveGet (toMeaningfullReference r) mn)
      $ tNodeReference n'
      
 
  setTNode m@(IsTNode a) n = do
    mn<-get
    n'<-n
    put $ selectivePut m mn
    return $ setTNodeReference (TNR $ getId a) n'
  
  setTNode _ n = n

  setTNode' a n = setTNode (IsTNode a) n



instance PosessingChosenMa NodeStructure where
  getChosen n = do 
    mn<-get
    n'<-n
    return 
      $ maybe []
      (\r-> mapMaybe fromMeaningfull 
                     $ selectiveGet (toMeaningfullReference r) mn)
      $ chosenReference n'
      
 
  setChosen m@(IsChosenNode a) n = do
    mn<-get
    n'<-n
    put $ selectivePut m mn
    return $ setChosenReference (CsnR $ getId a) n'
  
  setChosen _ n = n

  setChosen' a n = setChosen (IsChosenNode a) n







instance ContainingTextFillinReferences () where
  textReferences _ = []
  consTextReference _ n = n
  

  
instance ContainingTextFillinReferences Meaningfull where
  textReferences (IsBNode n) = textReferences n
  textReferences (IsGoto  n) = textReferences n
  textReferences (IsAltContentNode  n) = textReferences n
  textReferences (IsChosenNode n) = textReferences n
  textReferences _ = []
  consTextReference t (IsBNode n) = toMeaningfull 
                                                 $ consTextReference t n
  consTextReference t (IsGoto  n) = toMeaningfull 
                                                 $ consTextReference t n
  consTextReference t (IsAltContentNode  n) = toMeaningfull 
                                                 $ consTextReference t n
  consTextReference t (IsChosenNode n) = toMeaningfull 
                                                 $ consTextReference t n                                            
  consTextReference _ n = n
  



applyTemplateT_MeaningfullCommonM n = do 
  n'<-n
  applyTemplateMT $ return $ toMeaningfull n'

applyTemplate_MeaningfullCommonM n = do
  n'<-n
  c<-applyTemplateT n
  return $ putContent c n'

instance TemplateApplicable NodeStructure where
  applyTemplateT = applyTemplateT_MeaningfullCommonM
  applyTemplate  = applyTemplate_MeaningfullCommonM
 
 
applyTemplateMT ::  (--PosessingContent co
                 --,AcceptingTextRelated co
                 MonadState MeaningfullNodes m 
                 ,PosessingTextRelatedMa m
                  ) => 
                -- NodeStructure Meaningfull -> NodeStructure T.Text 
                 m Meaningfull -> m T.Text 
applyTemplateMT n = do 
  n'<-n
  getTextRelated_n<-getTextRelatedMa n
  
  let content = T.concat 
                  $ processIfWasSplit
                  $ zip (T.splitOn "${}" $ symbolsToTags $ getContent n')
                        $ (concat [sortOn (\t-> t^.tx_position) 
                                          $ nub getTextRelated_n
                                  , repeat $ Tx{_tx_name=""
                                              ,_tx_id=""
                                              ,_tx_content=""
                                              ,_tx_link=Nothing
                                              ,_tx_position=Nothing}
                                  ])
  
  return $ foldl' foldStep content 
                    $ map perTextFillIn getTextRelated_n
  where
  foldStep content f = f content
  
  perTextFillIn :: TextFillIn -> (T.Text->T.Text)
  perTextFillIn t = 
    T.replace (addBrackets $ t^.tx_name)
              (t^.tx_content)
  
  addBrackets :: T.Text -> T.Text 
  addBrackets txt = T.concat ["${", txt, "}"]
  
  symbolsToTags s = T.replace "&gt;" ">" $ T.replace "&lt;" "<" s
    -- T.replace ">" "LLL" $ T.replace "<" "GGG" $ T.replace "&gt;" "ggg" $ T.replace "&lt;" "lll" s

  processIfWasSplit [(c,_)] = [c]
  processIfWasSplit l = map (\(c,t)-> T.concat [c,t^.tx_content]) l





instance TextFillInLike TextFillIn where
  asTextFillIn t = Just t

instance TextFillInLike Meaningfull where
  asTextFillIn (IsTextFillIn t) = Just t
  asTextFillIn _ = Nothing
  
  

instance PosessingTextRelatedMa NodeStructure where
  getTextRelatedMa n = do 
    mn<-get
    n'<-n
    return 
      $ concat 
      $ mapMaybe
        (\txr_id-> 
          maybe Nothing 
            (\id-> DM.lookup id $ mn^.textFillIn)
            $ id_ txr_id
        ) 
        $ textReferences n'
    where
    id_ (TxR x) = Just x
    id_ _       = Nothing
    
    

instance PosessingContent BNode where
  getContent n = n^.bn_content
  putContent c n = set bn_content c n
  
instance PosessingContent Goto where
  getContent n = n^.g_option
  putContent c n = set g_option c n

instance PosessingContent Chosen where
  getContent n = n^.csn_option
  putContent c n = set csn_option c n

instance PosessingContent AltContent where
  getContent n = n^.ac_content
  putContent c n = set ac_content c n
   
  
instance PosessingContent Meaningfull where
  getContent (IsBNode n) = getContent n
  getContent (IsGoto  n) = getContent n
  getContent (IsChosenNode n) = getContent n
  getContent (IsAltContentNode  n) = getContent n
  getContent _ = ""
  
  putContent c (IsBNode n) = toMeaningfull $ putContent c n
  putContent c (IsGoto  n) = toMeaningfull $ putContent c n
  putContent c (IsChosenNode n) = toMeaningfull $ putContent c n
  putContent c (IsAltContentNode  n) = toMeaningfull $ putContent c n
  putContent _ n = n
  
  
instance PosessingPropertyDisplayOnNewPage BNode where
  ifDisplayOnNewPage n = n^.bn_displayOnNewPage
  setDisplayOnNewPage (IsDisplayOnNewPageNode _) n 
                                       = set bn_displayOnNewPage True n
  setDisplayOnNewPage _ n = n                          

instance PosessingPropertyDisplayOnNewPage Meaningfull where
  ifDisplayOnNewPage (IsBNode n) = ifDisplayOnNewPage n
  ifDisplayOnNewPage _ = False
  setDisplayOnNewPage d (IsBNode n) = IsBNode $ setDisplayOnNewPage d n
  setDisplayOnNewPage _ n = n
  
instance PosessingPropertyDisplayOnNewPage (NodeStructure BNode) where
  --ifDisplayOnNewPage (n,f) = f $ (return.ifDisplayOnNewPage) =<< n
  setDisplayOnNewPage d n = (return.setDisplayOnNewPage d) =<< n

instance PosessingPropertyDisplayOnNewPage (NodeStructure Meaningfull) where
  --ifDisplayOnNewPage n = (return.ifDisplayOnNewPage) =<< n
  setDisplayOnNewPage d n = do 
    n'<-n
    case n' of
      IsBNode n'' -> return $ IsBNode $ setDisplayOnNewPage d n''
      _           -> n
 

instance PosessingPropertyRandomNext BNode where
  ifRandomNext n = n^.bn_randomNext
  setRandomNext (IsRandomNext _) n = set bn_randomNext True n
  setRandomNext _ n = n                          

instance PosessingPropertyRandomNext Meaningfull where
  ifRandomNext (IsBNode n) = ifRandomNext n
  ifRandomNext _ = False
  setRandomNext d (IsBNode n) = IsBNode $ setRandomNext d n
  setRandomNext _ n = n
  
instance PosessingPropertyRandomNext (NodeStructure BNode) where
  setRandomNext d n = (return.setRandomNext d) =<< n
  

instance PosessingPropertyRandomNext (NodeStructure Meaningfull) where
  setRandomNext d n = do 
    n'<-n
    case n' of
      IsBNode n'' -> return $ IsBNode $ setRandomNext d n''
      _           -> n


instance PosessingPropertyOverrideNext BNode where
  getOverrideNext n = n^.bn_overrideNext
  setOverrideNext (IsOverrideNext o) n = set bn_skipPastNext 
                                             (o^.ovn_skips) 
                                            $ set bn_overrideNext 
                                                (Just $ o^.ovn_override) 
                                                n
  setOverrideNext _ n = n                          

instance PosessingPropertyOverrideNext Meaningfull where
  getOverrideNext (IsBNode n) = getOverrideNext n
  getOverrideNext _ = Nothing
  setOverrideNext d (IsBNode n) = IsBNode $ setOverrideNext d n
  setOverrideNext _ n = n
  
instance PosessingPropertyOverrideNext (NodeStructure BNode) where
  setOverrideNext d n = (return.setOverrideNext d) =<< n

instance PosessingPropertyOverrideNext (NodeStructure Meaningfull) where
  setOverrideNext d n = do 
    n'<-n
    case n' of
      IsBNode n'' -> return $ IsBNode $ setOverrideNext d n''
      _           -> n
 
 
 
instance PosessingPropertySkipPastNext BNode where
  getSkipPastNext n = n^.bn_skipPastNext
  setSkipPastNext (IsOverrideNext o) n = set bn_skipPastNext 
                                             (o^.ovn_skips) 
                                             n
  setSkipPastNext _ n = n                          

instance PosessingPropertySkipPastNext Meaningfull where
  getSkipPastNext (IsBNode n) = getSkipPastNext n
  getSkipPastNext _ = Nothing
  setSkipPastNext d (IsBNode n) = IsBNode $ setSkipPastNext d n
  setSkipPastNext _ n = n
  
instance PosessingPropertySkipPastNext (NodeStructure BNode) where
  setSkipPastNext d n = (return.setSkipPastNext d) =<< n

instance PosessingPropertySkipPastNext (NodeStructure Meaningfull) where
  setSkipPastNext d n = do 
    n'<-n
    case n' of
      IsBNode n'' -> return $ IsBNode $ setSkipPastNext d n''
      _           -> n
 
 

  
putTextRelatedNodeStructure :: (
                                 TextFillInLike t 
                                 ,ContainingTextFillinReferences n
                                 )
                                 => t
                                 -> NodeStructure n
                                 -> NodeStructure n
putTextRelatedNodeStructure t n' = putTR $ asTextFillIn t
  where
  putTR (Just t) = do 
    mn<-get
    n<-n'
    put $ set textFillIn 
              (DM.insertWith (++) (getId t) [t] $ mn^.textFillIn) 
              mn
    return $ consTextReference (TxR $ getId t) n
  
  putTR _ = n'

  

instance AcceptingTextRelated (NodeStructure BNode) where
  putTextRelated t n = putTextRelatedNodeStructure t n
    
instance AcceptingTextRelated (NodeStructure Goto) where
  putTextRelated t n = putTextRelatedNodeStructure t n
  
instance AcceptingTextRelated (NodeStructure AltContent) where
  putTextRelated t n = putTextRelatedNodeStructure t n

instance AcceptingTextRelated (NodeStructure Chosen) where
  putTextRelated t n = putTextRelatedNodeStructure t n
  
instance AcceptingTextRelated (NodeStructure Meaningfull) where
  putTextRelated t n = do
    n'<-n
    let putTR t n = (\x->return $ toMeaningfull x) 
                    =<< (putTextRelated t (return n))
    case n' of
      IsBNode n'' -> putTR t n''
      IsGoto  n'' -> putTR t n''
      IsAltContentNode n'' -> putTR t n''
      IsChosenNode n''     -> putTR t n''
      _           -> n
                    
  
instance PosessingConnectionToOtherBrach BNode where
  getConnections n = n^.bn_branchRelated
    
  putConnection n c = set bn_branchRelated (c:(n^.bn_branchRelated)) n
    
  putConnections n c = set bn_branchRelated 
                           (c ++ (n^.bn_branchRelated)) 
                           n
        
instance PosessingConnectionToOtherBrach TNode where
  getConnections n = n^.tn_branchRelated
    
  putConnection n c = set tn_branchRelated (c:(n^.tn_branchRelated)) n
    
  putConnections n c = set tn_branchRelated 
                           (c ++ (n^.tn_branchRelated)) 
                           n

instance PosessingConnectionToOtherBrach Meaningfull where
  getConnections (IsBNode n) = getConnections n
  getConnections (IsTNode n) = getConnections n
  getConnections _ = []
  
  putConnection (IsBNode n) c = toMeaningfull $ putConnection n c
  putConnection (IsTNode n) c = toMeaningfull $ putConnection n c
  putConnection n _ = n
  


instance (PosessingConnectionToOtherBrach n
         ,OneOfMeaningfull n
         ,HasId n) 
         => CanBuildConnectionToOtherBrach (NodeStructure n) where
  addConnection mm@(IsGoto m) n = do
    n'<-n
    getById <- getOtherBranchConnectedToTheSameGoto

    (\b-> do 
            let nl = newLink m b
            noteV "all" 
              $ unwords ["addConnection",T.unpack $ getId n',show nl]
            return $ putConnection n' nl {- $ newLink m b-}) 
      $ getById $ getId m
          
    where
    newLink m (Just b) = ConnectionOverGoto (GR $ getId m) (BNR $ getId b)
    newLink m Nothing = HangingConnection (GR $ getId m)
    
    getOtherBranchConnectedToTheSameGoto = do 
      mn<-get
       
      return $ (\gotoid-> listToMaybe 
        $ mapMaybe toBNode
        $ concatMap (\id-> selectiveGet 
                                   (toMeaningfullReference $ BNR id) mn) 
        $ mapMaybe (nextId gotoid) $ mn^.connections
       )
      where
      nextId id c 
        |(c^.cn_start==id) && (c^.cn_end/=id) = Just $ c^.cn_end
        |otherwise = Nothing
  
      toBNode (IsBNode b) = Just b
      toBNode _ = Nothing
  
  addConnection (IsBNode m) n = do
    n'<-n
    return $ putConnection n' $ DirectConnection $ BNR $ getId m
                 

  addConnection _ n = n
  

  
instance CanBuildConnectionToOtherBrach (NodeStructure Meaningfull) where
  addConnection m n = do
    n'<-n
    
    let run n'' = do
                      n'''<-addConnection m $ return n''
                      return $ toMeaningfull n'''
    case n' of
      IsBNode n'' -> run n''
      IsTNode n'' -> run n''           
      _           -> n

  
  
  
 

