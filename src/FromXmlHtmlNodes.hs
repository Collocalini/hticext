{-# LANGUAGE TemplateHaskell #-}
-- {-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DeriveGeneric #-}

-- {-#  LANGUAGE FlexibleInstances #-}
-- {-#  LANGUAGE FlexibleContexts #-}
-- {-#  LANGUAGE TypeSynonymInstances #-}


module FromXmlHtmlNodes (
 nodeToMeaningfull
,nodeToTextFillIn
,nodeToGoto
,nodeToDisplayOnNewPage
,nodeToBNode
,getAllConnectors
,textFillInApplyExternalLink
) where


import qualified Data.ByteString as B'
import Data.Maybe
import qualified Data.Map as DM
import qualified Data.Text as T
import Control.Lens hiding ((|>),(<.>))
import qualified Control.Lens as L --hiding (element)
import Control.Monad.State.Lazy
import qualified Control.Monad.State.Lazy as St
import Control.Monad.RWS.Lazy
import qualified Control.Monad.RWS.Lazy as Rws
import GHC.Generics (Generic, Generic1)

import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr
                         ,replicate)
import Text.XmlHtml


import Types
import Instances


L.makeLenses ''Branch
L.makeLenses ''BNode
L.makeLenses ''TNode
L.makeLenses ''Connection
L.makeLenses ''DisplayOnNewPageNode
L.makeLenses ''Point
L.makeLenses ''TextFillIn
L.makeLenses ''Goto
L.makeLenses ''MeaningfullNodes
L.makeLenses ''HticConfig
L.makeLenses ''AltContent
L.makeLenses ''RunCode
L.makeLenses ''Chosen




getAllConnectors :: [Node] -> [Connection]
getAllConnectors ns = 
  catMaybes
    $ map nodeToConnector
    $ filter (\n-> (tagName n) == (Just "draw:connector") ) ns 

  where
  nodeToConnector n 
   |(isJust $ start n, isJust $ end n) == (True,True) = 
     Just $ Cn {_cn_start=fromJust $ start n, _cn_end=fromJust $ end n}
   |otherwise = Nothing 
 
  start n = getAttribute "draw:start-shape" n
  end   n = getAttribute "draw:end-shape"   n





nodeToBNode :: Node -> Maybe BNode
nodeToBNode n
  |isBNode = Just $ BN {_bn_name=fromJust getId
                         ,_bn_content=fromMaybe "" $ getDesc n
                         ,_bn_textRelated=[]
                         ,_bn_branchRelated=[]
                         ,_bn_displayOnNewPage=False
                         ,_bn_altContent=Nothing
                         ,_bn_randomNext=False
                         ,_bn_autoContinue=Nothing
                         ,_bn_overrideNext=Nothing
                         ,_bn_skipPastNext=Nothing
                         ,_bn_template=Nothing
                         ,_bn_runCode=Nothing} 
  |otherwise = Nothing  
  where
  isBNode = (titleSays "node" n) && (isJust getId)
  getId = getAttribute "draw:id" n
  
  
nodeToTNode :: Node -> Maybe TNode
nodeToTNode n
  |isTNode = Just $ TN {_tn_id=fromJust getId
                         ,_tn_branchRelated=[]} 
  |otherwise = Nothing  
  where
  isTNode = (titleSays "tnode" n) && (isJust getId)
  getId = getAttribute "draw:id" n


nodeToAltContent :: Node -> Maybe AltContent
nodeToAltContent n
  |isAltContent = Just $ AC {_ac_id=fromJust getId
                            ,_ac_content=fromMaybe "" $ getDesc n
                            ,_ac_textRelated=[]
                            ,_ac_timeout=timeout} 
  |otherwise = Nothing  
  where
  isAltContent = (titleSays "altContent" n) && (isJust getId)
  getId = getAttribute "draw:id" n
  timeout = altContentTimeoutTextToWord $ getText n


nodeToAutoContinue :: Node -> Maybe AutoContinue
nodeToAutoContinue n
  |isAutoContinue = Just $ AUC {_auc_id=fromJust getId
                               ,_auc_timeout=timeout} 
  |otherwise = Nothing  
  where
  isAutoContinue = (titleSays "autoContinue" n) && (isJust getId)
  getId = getAttribute "draw:id" n
  timeout = altContentTimeoutTextToWord $ getText n



nodeToDisplayOnNewPage :: Node -> Maybe DisplayOnNewPageNode
nodeToDisplayOnNewPage n
  |isDisplayOnNewPage = Just 
                        $ DisplayOnNewPageNode {_donp_id=fromJust getId} 
  |otherwise = Nothing  
  where
  isDisplayOnNewPage = (titleSays "displayOnNewPage" n) && (isJust getId)
  getId = getAttribute "draw:id" n


nodeToRandomNext :: Node -> Maybe RandomNext
nodeToRandomNext n
  |isRandomNext = Just $ RandomNext {_rndnxt_id=fromJust getId} 
  |otherwise = Nothing  
  where
  isRandomNext = (titleSays "randomNext" n) && (isJust getId)
  getId = getAttribute "draw:id" n


nodeToOverrideNext :: Node -> Maybe OverrideNext
nodeToOverrideNext n
  |isOverrideNext = Just $ OVN {_ovn_id=fromJust getId
                               ,_ovn_override=fromJust $ override 
                                                       $ getText n
                               ,_ovn_skips = getDesc n } 
  |otherwise = Nothing  
  where
  isOverrideNext = (titleSays "overrideNext" n) 
                     && (isJust getId) 
                     && (isJust $ override $ getText n)
  getId = getAttribute "draw:id" n
  override "" = Nothing
  override x  = Just x




nodeToGoto :: Node -> Maybe Goto
nodeToGoto n
  |isGoto = Just $ G {_g_id=fromJust getId
                     ,_g_option=fromMaybe "" $ getDesc n
                     ,_g_related=[]
                     ,_g_chosen=Nothing
                     ,_g_mark = getMark $ getText n
                     ,_g_position = getPos (getX n,getY n)} 
  |otherwise = Nothing  
  where
  isGoto = (titleSays "goto" n) && (isJust getId)
  getId = getAttribute "draw:id" n
  
  getMark x 
    |(T.strip x) /= "" = Just x
    |otherwise      = Nothing
  
  
  
nodeToChosen :: Node -> Maybe Chosen
nodeToChosen n
  |isGoto = Just $ Csn {_csn_id=fromJust getId
                       ,_csn_option=fromMaybe "" $ getDesc n
                       ,_csn_related=[]} 
  |otherwise = Nothing  
  where
  isGoto = (titleSays "chosen" n) && (isJust getId)
  getId = getAttribute "draw:id" n
  



nodeToRunCode :: Node -> Maybe RunCode
nodeToRunCode n
  |isRunCode = Just $ RnC {_rnc_id=fromJust getId
                          ,_rnc_content=toCode $ getText n
                          ,_rnc_replaceWithBID=getDesc n
                          } 
  |otherwise = Nothing  
  where
  isRunCode = (titleSays "runCode" n) && (isJust getId)
  getId = getAttribute "draw:id" n
  toCode txt = 
    T.replace "&gt;" ">" 
      $ T.replace "&lt;" "<" 
      $ T.replace "‘" "'"
      $ T.replace "’" "'"
      $ T.replace "”" "\""
      $ T.replace "“" "\"" txt





parseDrawTransform n (Just t) 
  |T.isInfixOf "translate" $ T.toLower t = Just
    $ take 2 $ drop 1 $ dropWhile notTranslate $ T.words $ T.toLower t 
  |otherwise = Nothing
  where
  notTranslate "translate" = False
  notTranslate _ = True
  
  
parseDrawTransform _ _ = Nothing


getX n = listToMaybe $ catMaybes 
         [getAttribute "svg:x" n
         ,maybe Nothing
            (\[x,_]-> Just x)
            $ parseDrawTransform n
            $ getAttribute "draw:transform" n
         ]
getY n = listToMaybe $ catMaybes 
       [getAttribute "svg:y" n
       ,maybe Nothing
          (\[_,y]-> Just y)
          $ parseDrawTransform n
          $ getAttribute "draw:transform" n
       ]
getPos (Just sx, Just sy) = Just $ Point 
                                  {_pt_x = gotoPostextToDouble sx
                                  ,_pt_y = gotoPostextToDouble sy
                                  }
getPos _ = Nothing 



gotoPostextToDouble t = read $ T.unpack $ T.filter (oneOfallowed) t
  where
  allowed = "0123456789.,"
  oneOfallowed x = T.isInfixOf (T.singleton x) allowed


altContentTimeoutTextToWord t = parseIfGood $ T.filter (oneOfallowed) t
  where
  allowed = "0123456789"
  oneOfallowed x = T.isInfixOf (T.singleton x) allowed
  parseIfGood "" = Nothing
  parseIfGood x = Just $ read $ T.unpack x
  
 
getDesc n = maybe Nothing
  (\t-> listToMaybe $ map nodeText $ filter isTextNode $ childNodes t) 
  $ childElementTag "svg:desc" n 





titleSays x n = maybe False 
    (\t-> maybe False 
                (\t1-> x == nodeText t1) 
                $ listToMaybe $ filter isTextNode $ childNodes t
    ) 
     $ childElementTag "svg:title" n



getTextOfTagsStartingWith tagStart n = T.concat 
    $ map nodeText 
          $ filter (tagStartsWith tagStart . tagName) 
                   $ childNodes n 

   
getText n = getTextOfTagsStartingWith "text" n 
getBody n = getTextOfTagsStartingWith "body" n 

tagStartsWith x (Just tag) = x == (T.take (T.length x) tag)
tagStartsWith x _ = False





nodeToTextFillIn :: Node -> Maybe TextFillIn
nodeToTextFillIn n
  |isTextFillIn = Just $ Tx {_tx_name=fromMaybe "x" getName
                            ,_tx_id=fromJust getId
                            ,_tx_content=getText n
                            ,_tx_link=getDesc n
                            ,_tx_position=getPos (getX n,getY n)} 
  |otherwise = Nothing  
  where
  isTextFillIn = (titleSays "text" n) && (isJust getId) -- && (isJust getName)
  getId = getAttribute "draw:id" n
  getName = getAttribute "draw:name" n

  


textFillInApplyExternalLink :: TextFillIn -> IO TextFillIn
textFillInApplyExternalLink t = do 
  maybe (return t)
    (\ref-> do
      tx_linkF<- B'.readFile $ T.unpack ref

      case (parseHTML (T.unpack $ fromJust $ t^.tx_link) tx_linkF)of
        Left e -> do putStrLn e
                     return t
        Right tx_linkFd@(HtmlDocument {docContent =tx_linkF_dc}) -> do 
          let body = T.concat $ map getBody tx_linkF_dc
          case body of
            "" -> return t
            b  -> return $ set tx_content b t
    )
    $ t^.tx_link
    
  


nodeToMeaningfull :: Node -> Maybe Meaningfull
nodeToMeaningfull n = listToMaybe $ catMaybes $ map (\f-> f n) 
         [
          maybe Nothing (Just . toMeaningfull).nodeToBNode
         ,maybe Nothing (Just . toMeaningfull).nodeToTNode
         ,maybe Nothing (Just . toMeaningfull).nodeToGoto
         ,maybe Nothing (Just . toMeaningfull).nodeToTextFillIn
         ,maybe Nothing (Just . toMeaningfull).nodeToDisplayOnNewPage
         ,maybe Nothing (Just . toMeaningfull).nodeToAltContent
         ,maybe Nothing (Just . toMeaningfull).nodeToRandomNext
         ,maybe Nothing (Just . toMeaningfull).nodeToAutoContinue
         ,maybe Nothing (Just . toMeaningfull).nodeToOverrideNext
         ,maybe Nothing (Just . toMeaningfull).nodeToRunCode
         ,maybe Nothing (Just . toMeaningfull).nodeToChosen
         ]
 

