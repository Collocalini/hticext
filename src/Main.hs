{-# LANGUAGE TemplateHaskell #-}
-- {-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DeriveGeneric #-}

-- {-#  LANGUAGE FlexibleInstances #-}
{-#  LANGUAGE FlexibleContexts #-}
-- {-#  LANGUAGE TypeSynonymInstances #-}


import System.Environment
import qualified Data.Set as S
import qualified Data.Map as DM
import qualified Data.ByteString.Lazy.Char8 as B8 
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString as B'
import qualified Data.Binary.Builder as BB
import Data.List
import Data.Maybe
import Data.Either
import Data.Word
import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr
                         ,replicate)
import qualified Data.DList as DL
import Text.XmlHtml
import qualified Data.Text as T
import Control.Lens hiding ((|>),(<.>),chosen)
import qualified Control.Lens as L --hiding (element)
import Control.Monad.State.Lazy
import qualified Control.Monad.State.Lazy as St
import Control.Monad.RWS.Lazy
import qualified Control.Monad.RWS.Lazy as Rws
--import GHC.Generics (Generic, Generic1)

import Data.Aeson hiding ((.=))
import Data.Aeson.Lens hiding ((.=))

--type State s = StateT s Identity

import Types  
import FromXmlHtmlNodes
import ToXmlHtmlNodes
import Instances
import Utils

L.makeLenses ''Branch
L.makeLenses ''BNode
L.makeLenses ''TNode
L.makeLenses ''Connection
L.makeLenses ''DisplayOnNewPageNode
L.makeLenses ''Point
L.makeLenses ''TextFillIn
L.makeLenses ''Goto
L.makeLenses ''MeaningfullNodes
L.makeLenses ''HticConfig
L.makeLenses ''AltContent
L.makeLenses ''AutoContinue
L.makeLenses ''RunCode
L.makeLenses ''Chosen

data PerBranchCommon = PerBranchCommon 
  {_cont :: T.Text
  ,_donp :: Bool
  ,_nxt  :: [T.Text]
  ,_altc :: Maybe T.Text
  ,_altct :: Maybe Word
  ,_rndnxt :: Bool
  ,_auc :: Maybe Word
  ,_ovdnxt :: Maybe T.Text
  ,_skpnxt :: Maybe T.Text
  ,_tn :: [Template]
  ,_rnc :: Maybe T.Text
  }
L.makeLenses ''PerBranchCommon

  





main = do
   a<-getArgs
   j<- readFile $ last a
   let inputFile = fromMaybe "input.fodg" 
                                      $ j ^? key "inputFile"    . _JSON
   let conf = HticConfig {
       _hticConf_inputFile    = inputFile
      ,_hticConf_outputFile   = fromMaybe (inputFile ++ ".html")
                                      $ j ^? key "outputFile"   . _JSON                              
      ,_hticConf_templateFile = fromMaybe "tmp.html"
                                      $ j ^? key "templateFile" . _JSON
      ,_hticConf_verbosity    = fromMaybe ["all"]
                                      $ j ^? key "verbosity"    . _JSON
      }
   fodg<- B'.readFile $ conf^.hticConf_inputFile
   tmphtml<- B'.readFile $ conf^.hticConf_templateFile
      
   let etherTmpHTML = parseHTML (conf^.hticConf_templateFile) tmphtml
   let etherFodg    = parseXML  (conf^.hticConf_inputFile)    fodg

   case ((etherFodg,etherTmpHTML))of
      (Right dfodg@(XmlDocument {docContent =dfodg_dc}) 
       ,Right dtmp
       ) -> do 
        
        let allNodes = concatMap descendantNodes dfodg_dc
        let allConnectors = getAllConnectors allNodes
        
        noteVIO "showAllConnectors" conf "showAllConnectors"
        mapM_ (\c-> noteVIO "showAllConnectors" conf $ show c
              ) allConnectors
        noteVIO "showAllConnectors" conf "showAllConnectors end"
        
        let meaningfullNodes_starting = MeaningfullNodes
                                          {_bnodes = DM.empty
                                          ,_tnodes = DM.empty
                                          ,_goto   = DM.empty
                                          ,_textFillIn  = DM.empty
                                          ,_propDisplayOnNewPage = DM.empty
                                          ,_altContent = DM.empty
                                          ,_randomNext = DM.empty
                                          ,_autoContinue = DM.empty
                                          ,_overrideNext = DM.empty
                                          ,_runCode      = DM.empty
                                          ,_chosen       = DM.empty
                                          ,_connections = allConnectors
                                          }

        
        
        (\allConnected-> do 
          
          noteVIO "showAllConnected" conf "showAllConnected"
          mapM_ (\c-> noteVIO "showAllConnected" conf $ show c
                ) allConnected
          noteVIO "showAllConnected" conf "showAllConnected end"
          
          let (Identity (_,meaningfullNodes,w)) = initialParse 
                                              allConnected
                                              conf 
                                              meaningfullNodes_starting
            
          mapM_ (putStrLn . logRecordToString) w
          
          noteVIO "show_meaningfullNodes_after_initialParse" 
                  conf 
                  "show_meaningfullNodes_after_initialParse"
          mapM_ (\c-> noteVIO "show_meaningfullNodes_after_initialParse" 
                              conf
                              $ show c
                ) $ meaningfullNodes^.bnodes
          mapM_ (\c-> noteVIO "show_meaningfullNodes_after_initialParse" 
                              conf
                              $ show c
                ) $ meaningfullNodes^.tnodes
          mapM_ (\c-> noteVIO "show_meaningfullNodes_after_initialParse" 
                              conf
                              $ show c
                ) $ meaningfullNodes^.goto
          mapM_ (\c-> noteVIO "show_meaningfullNodes_after_initialParse" 
                              conf
                              $ show c
                ) $ meaningfullNodes^.textFillIn
          mapM_ (\c-> noteVIO "show_meaningfullNodes_after_initialParse" 
                              conf
                              $ show c
                ) $ meaningfullNodes^.connections
          noteVIO "show_meaningfullNodes_after_initialParse" 
                  conf 
                  "show_meaningfullNodes_after_initialParse end"
          
          
             
          tfl<- mapM (\(k,tl)-> do 
                       tl'<-mapM textFillInApplyExternalLink tl
                       return (k,tl')
                     ) 
                     $ DM.toList $ meaningfullNodes^.textFillIn
          
        
          (\(Identity (v,w))-> do 
              mapM_ (putStrLn . logRecordToString) w
              B.writeFile (conf^.hticConf_outputFile) v
            ) 
            $ evalNodeStructure
              (do 
                allConnectedAttributed<- attributeConnected allConnected
                
                sortByType allConnectedAttributed
                
                mn<-get
                noteV "all" $ unlines 
                  $ map (unlines . map ((++) "\n - " .show).snd) 
                  $ DM.toList $ mn^.bnodes
                noteV "all" $ unlines 
                  $ map ((++) "\n - " .show)
                  $ sort
                  $ concat
                  $ map snd
                  $ DM.toList $ mn^.goto
                noteV "all" $ unlines $ map show 
                  $ mn^.connections
                noteV "all" $ unlines 
                  $ map (unlines . map ((++) "\n - " .show).snd) 
                  $ DM.toList $ mn^.randomNext
                  
                (\branches-> return
                  $ BB.toLazyByteString
                  $ render
                  $ (\d-> documentReplaceDisplay d 
                      $ concatMap 
                        (displayableNodeFromBranch
                         (DM.fromList $ map (\b-> (b^.b_name,b)) branches)
                        ) 
                      $ take 1 branches
                    ) 
                  $ documentReplaceData dtmp 
                  $ map nodeFromBranch branches
                  ) 
                  =<< (\s-> do 
                               noteV "all" $ unlines 
                                 $ map (show) $ S.toList s
                               buildData s
                      ) 
                  =<< assureUniqueBranches
              ) 
              conf
              $ set textFillIn (DM.fromList tfl) meaningfullNodes
          )
          $ getAllConnected allConnectors allNodes
          
          
  
      
      _ -> putStrLn "can't parse"
      
   

   where
   attributeConnected [] = return []
   attributeConnected (ac:rest) = do 
     n<- attributeAdjacentToANode' ac
     ac'<-applyTemplate $ return n
     rest'<-attributeConnected rest
     return (ac':rest')

   logRecordToString (Note x) = x
   logRecordToString _ = ""

   initialParse allConnected 
                conf 
                meaningfullNodes_starting = runNodeStructure
            (do 
              sortByType allConnected
{- allConnected'<- -}    
              ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndingsM' allConnected
              return ""
            )
            conf
            meaningfullNodes_starting
      


   




getAllConnected :: [Connection] -> [Node] -> [Meaningfull]
getAllConnected cns ns = catMaybes $ map nodeToMeaningfull 
                                   $ filter idIsRelevant ns
  where
  getRelevantIds = concatMap (\c-> [c^.cn_start,c^.cn_end]) cns

  idIsRelevant n = maybe False (\x-> elem x getRelevantIds) 
                                              $ getAttribute "draw:id" n



sortByType :: MonadState MeaningfullNodes m => [Meaningfull] -> m ()
sortByType [] = return ()
sortByType (m:rest) = do
     selectivePutM m
     sortByType rest
  where
  selectivePutM x = do
    mn<-get
    put $ selectivePut x mn
       

attributeAdjacentToANode' :: --MonadState MeaningfullNodes m 
                             --  => 
                               Meaningfull 
                               -> NodeStructure Meaningfull
attributeAdjacentToANode' m = do 
  mn<-get
  
  go<-mapOverConnections (mn^.connections) $ return m
  return go
  where
  mapOverConnections :: --MonadState MeaningfullNodes m 
                        -- => 
                         [Connection] 
                         -> NodeStructure Meaningfull 
                         -> NodeStructure Meaningfull
  mapOverConnections [] node = node
  mapOverConnections (c:rest) node = do
    mn<-get
    
    node'<-node
   
    mapOverConnections rest 
      $ maybe node 
            (\id-> foldl' (\n m -> processConnected n id m) 
                         node 
                         $ lookupApplicableNode id mn
            )
            $ nodeIsConnectedToSomething node' c
    
  
  nodeIsConnectedToSomething (IsBNode n) c =
                                    nodeIsConnectedToSomethingCommon n c
                                    
  nodeIsConnectedToSomething (IsTNode n) c =
                                    nodeIsConnectedToSomethingCommon n c
                                    
  nodeIsConnectedToSomething (IsGoto n) c =
                                    nodeIsConnectedToSomethingCommon n c
  
  nodeIsConnectedToSomething (IsAltContentNode n) c =
                                    nodeIsConnectedToSomethingCommon n c
                                    
  nodeIsConnectedToSomething (IsChosenNode n) c =
                                    nodeIsConnectedToSomethingCommon n c
                                    
  nodeIsConnectedToSomething (IsRunCodeNode n) c =
                                    nodeIsConnectedToSomethingCommon n c
  
  nodeIsConnectedToSomething _ _ = Nothing
    
  nodeIsConnectedToSomethingCommon n c
    |c^.cn_start==(getId n) = Just $ ToA $ (toMeaningfull n,c^.cn_end)
    |c^.cn_end==(getId n) = Just $ FromA $ (toMeaningfull n,c^.cn_start)
    |otherwise = Nothing
  

  lookupApplicableNode (ToA (_,id))   mn = getAnyMeaningfullNode id mn
  lookupApplicableNode (FromA (_,id)) mn = getAnyMeaningfullNode id mn
  lookupApplicableNode _ _ = []
  
  processConnectedCommon ::  (--PosessingTextRelated n
                      -- ,PosessingConnectionToOtherBrach n
                     -- MonadState MeaningfullNodes m
                     
                      ) => 
                     NodeStructure Meaningfull 
                     -> DirectionAware Meaningfull
                     -> NodeStructure Meaningfull
  processConnectedCommon n dm = consumesConnected dm n
  
  processConnected n (ToA (_,_))   m = processConnectedCommon n (ToA m)
  processConnected n (FromA (_,_)) m = processConnectedCommon n (FromA m)
  processConnected n _ _ = n
  
  

  
defaultBranch = B 
  {_b_name = ""
  ,_b_option = ""
  ,_b_optionChosenState = Nothing
  ,_b_content = ""
  ,_b_next = []
  ,_b_displayOnNewPage = False
  ,_b_altContent = Nothing
  ,_b_altContentTimeout = Nothing
  ,_b_autoContinue = Nothing
  ,_b_randomNext = False
  ,_b_buildFromTamplate = []
  ,_b_overrideNext = Nothing
  ,_b_skipPastNext = Nothing
  }



buildData :: (MonadState MeaningfullNodes m 
             ,MonadWriter (DList ApplicationLog) m 
             ,MonadReader HticConfig m 
             )
              => (S.Set ConnectionOverSomething) 
              -> m [Branch]
buildData s = do
  mn<-get
  (return.catMaybes)=<< (mapM perBranch $ S.toList s)
  
  where
  
  
  buildBranch n
              tn
              opt 
              cont
              nxt
              donp
              altc
              altct
              rndnxt
              auc
              ovdnxt
              skpnxt
              rnc
              optCsn
               
    |isJust n = Just $ set b_name (fromMaybe "" n)
                     $ set b_buildFromTamplate tn
                     $ set b_option opt
                     $ set b_content cont
                     $ set b_displayOnNewPage donp
                     $ set b_altContent altc
                     $ set b_altContentTimeout altct
                     $ set b_randomNext rndnxt
                     $ set b_autoContinue auc
                     $ set b_overrideNext ovdnxt
                     $ set b_skipPastNext skpnxt
                     $ set b_runCode rnc
                     $ set b_optionChosenState optCsn
                     $ set b_next nxt defaultBranch
    |otherwise = Nothing

  
  sortConnections mn c = sortConnectionsCommon fst mn c 
  sortConnectionsOnConnection mn c = sortConnectionsCommon snd mn c 
  
  sortConnectionsCommon on mn c = map snd
      $ (sortOn on) 
      $ mapMaybe (\cn-> 
                   toTouple cn
                     $ maybe Nothing
                         (\gid->lookupSingle gid $ mn^.goto)
                         $ connectionOverSomethingToGID cn
                 )  
                 c
    where
    
    
    --toTouple :: ConnectionOverSomething 
    --         -> Maybe Goto 
    --         -> Maybe (ConnectionOverSomething,Goto)
    toTouple cn (Just g) = Just (g,cn)
    toTouple _ _ = Nothing
    

  {-sortConnections' mn c = map fst
      $ (sortOn fst) 
      $ mapMaybe (\cn-> 
                   toTouple cn
                     $ maybe Nothing
                         (\gid->lookupSingle gid $ mn^.goto)
                         $ connectionOverSomethingToGID cn
                 )  
                 c
    where
    connectionOverSomethingToGID (ConnectionOverGoto (GR gid) _) = 
                                             Just gid
    connectionOverSomethingToGID _ = Nothing
    
    --toTouple :: ConnectionOverSomething 
    --         -> Maybe Goto 
    --         -> Maybe (ConnectionOverSomething,Goto)
    toTouple cn (Just g) = Just (g,cn)
    toTouple _ _ = Nothing
  -}
  nxtBranches mn bid = mapMaybe connectionOverSomethingToID
                                $ xBranchesCommon mn 
                                                  bid 
                                                  bnodes
                                                  bn_branchRelated
                                                  (sortConnections mn)
                     
  tplBranches mn tid = mapMaybe (\x-> connectionOverSomethingToTpl mn x) 
                                --(\x-> Just $ TplCache "xxx")
                                $ xBranchesCommon mn 
                                                  tid 
                                                  tnodes
                                                  tn_branchRelated
                                              (sortConnections mn)
  
  xBranchesCommon mn xid mnGetter branchGetter sorter = maybe [] 
                     (\x-> sorter
                            $ filter (keepOtherBranches xid)
                            $ x^.branchGetter
                     )
                     $ lookupSingle xid $ mn^.mnGetter
  {-nxtBranches' mn bid = maybe [] 
                     (\x-> (sortConnections' mn)
                                    $ filter (keepOtherBranches bid)
                                    $ x^.bn_branchRelated
                     )
                     $ lookupSingle bid $ mn^.bnodes 
  
-}

  perBranchCommon bid = do
    mn<-get
    let cont = maybe "" (\x-> x^.bn_content) $ lookupSingle bid $ mn^.bnodes
    let nxt  = nxtBranches mn bid
    let donp = maybe False (\x-> x^.bn_displayOnNewPage) 
                                         $ lookupSingle bid $ mn^.bnodes
    
    let altcn = maybe Nothing 
                          fromMeaningfull
                  $ maybe Nothing
                     (\ar->listToMaybe 
                       $ selectiveGet (IsAltContentNodeReference ar) 
                                      mn
                     )
                     $ maybe Nothing
                     (\x-> x^.bn_altContent)
                                         $ lookupSingle bid $ mn^.bnodes
    let altc = maybe Nothing    
                  (\ac-> Just $ ac^.ac_content)
                  altcn
    let altct = maybe Nothing    
                  (\ac-> ac^.ac_timeout)
                  altcn
    let rndnxt = maybe False (\x-> x^.bn_randomNext) 
                                         $ lookupSingle bid $ mn^.bnodes
    let ovdnxt = maybe Nothing (\x-> x^.bn_overrideNext) 
                                         $ lookupSingle bid $ mn^.bnodes
    let skpnxt = maybe Nothing (\x-> x^.bn_skipPastNext) 
                                         $ lookupSingle bid $ mn^.bnodes                                     
    let tn = maybe [] 
                   (\x-> concatMap ((tplBranches mn).refId) 
                             --(\x->[TplData $ T.pack $ show $ refId x])
                             $ maybeToList $ x^.bn_template
                    ) 
                    $ lookupSingle bid $ mn^.bnodes
                    
    let auc = maybe Nothing    
                (\auc-> auc^.auc_timeout)
                $ maybe Nothing 
                        fromMeaningfull
                  $ maybe Nothing
                     (\ar->listToMaybe 
                       $ selectiveGet (IsAutoContinueReference ar) 
                                      mn
                     )
                     $ maybe Nothing
                     (\x-> x^.bn_autoContinue)
                                         $ lookupSingle bid $ mn^.bnodes
    
    let rnc = maybe Nothing    
                  (\rnc-> maybe 
                    (Just $ rnc^.rnc_content) 
                    (\replaceWithBID->maybe 
                      Nothing
                      (\gidbid -> Just 
                        $ T.replace 
                          replaceWithBID 
                          gidbid
                          $ rnc^.rnc_content
                      )
                      $ listToMaybe nxt
                    )
                    $ rnc^.rnc_replaceWithBID
                  )
                  $ maybe Nothing 
                          fromMeaningfull
                  $ maybe Nothing
                     (\rncr->listToMaybe 
                       $ selectiveGet (IsRunCodeReference rncr) 
                                      mn
                     )
                     $ maybe Nothing
                     (\x-> x^.bn_runCode)
                                         $ lookupSingle bid $ mn^.bnodes
    
    
    return $ PerBranchCommon 
      {_cont = cont
      ,_donp = donp
      ,_nxt  = nxt
      ,_altc = altc
      ,_altct = altct
      ,_rndnxt = rndnxt
      ,_auc = auc
      ,_ovdnxt = ovdnxt
      ,_skpnxt = skpnxt
      ,_tn = tn
      ,_rnc = rnc
      }
  
  perBranch :: (MonadState MeaningfullNodes m 
               ,MonadWriter (DList ApplicationLog) m 
               ,MonadReader HticConfig m 
               )
     => ConnectionOverSomething -> m (Maybe Branch)
  perBranch costh@(ConnectionOverGoto (GR gid) (BNR bid)) = do 
    mn<-get
    pbc <- perBranchCommon bid
    let n    = connectionOverSomethingToID costh
    let opt  = maybe "" (\x-> x^.g_option) -- $ manyGotoToOne 
                                           -- $ concat $ maybeToList 
                                           -- $ DM.lookup gid $ mn^.goto
                                           $ lookupSingle gid $ mn^.goto
    
    let optCsn  = do 
                    g <-lookupSingle gid $ mn^.goto
                    (CsnR csnid) <- g^.g_chosen
                    csn <-lookupSingle csnid $ mn^.chosen
                    Just $ csn^.csn_option
                    
    
    
    
  --  mapM_ (\c->noteV "all" $ ((++) (T.unpack bid) . (++) "-nxt-" .show) c) nxt
  --  mapM_ (\c->noteV "all" $ ((++) (T.unpack bid) . (++) "-nxt-" .show) c) 
  --   $ nxtBranches' mn bid
     
    return $ buildBranch 
              n
              (pbc^.tn)
              opt 
              (pbc^.cont)
              (pbc^.nxt)
              (pbc^.donp)
              (pbc^.altc)
              (pbc^.altct)
              (pbc^.rndnxt)
              (pbc^.auc)
              (pbc^.ovdnxt)
              (pbc^.skpnxt)
              (pbc^.rnc)
              optCsn
              
    
  perBranch costh@(DirectConnection (BNR bid)) = do 
   mn<-get
   pbc <- perBranchCommon bid
   let n    = Just bid
   let opt  = bid
   
  -- mapM_ (\c->noteV "all" $ ((++) (T.unpack bid) . (++) "-nxt-" .show) c) nxt
  -- mapM_ (\c->noteV "all" $ ((++) (T.unpack bid) . (++) "-nxt-" .show) c) 
  --   $ nxtBranches' mn bid


   return $ buildBranch 
           n
           (pbc^.tn)
           opt 
           (pbc^.cont)
           (pbc^.nxt)
           (pbc^.donp)
           (pbc^.altc)
           (pbc^.altct)
           (pbc^.rndnxt)
           (pbc^.auc)
           (pbc^.ovdnxt)
           (pbc^.skpnxt)
           (pbc^.rnc)
           Nothing
           
  perBranch _ = return Nothing

  keepOtherBranches thisBranchId 
                    g@(ConnectionOverGoto _ (BNR bid)) 
    |thisBranchId /= bid = True
    |otherwise           = False
  
  keepOtherBranches _ (DirectConnection _) = True
  keepOtherBranches _ (HangingConnection _) = True
  
  keepOtherBranches _ _  = False

lookupSingle id d = listToMaybe $ concat $ maybeToList $ DM.lookup id d

connectionOverSomethingToGID (ConnectionOverGoto (GR gid) _) = Just gid
connectionOverSomethingToGID (HangingConnection (GR gid) ) = Just gid
connectionOverSomethingToGID _ = Nothing

connectionOverSomethingToID (ConnectionOverGoto (GR gid) (BNR bid)) = 
                                             Just $ T.concat [gid,bid]
connectionOverSomethingToID _ = Nothing

connectionOverSomethingToTpl mn (ConnectionOverGoto (GR gid) (BNR bid)) = 
                                    Just $ TplData $ T.concat [gid,bid]
connectionOverSomethingToTpl mn (HangingConnection (GR gid)) = 
  maybe Nothing 
        (\g-> Just $ TplCache $ g^.g_option) 
        $ lookupSingle gid $ mn^.goto
  
connectionOverSomethingToTpl _ _ = Nothing

assureUniqueBranches :: (
                --  PosessingConnectionToOtherBrach n, 
                  MonadState MeaningfullNodes m 
                 ,MonadReader HticConfig m 
                 ,MonadWriter (DList ApplicationLog) m 
                )
                         => m (S.Set ConnectionOverSomething)
assureUniqueBranches = do 
  mn<-get
  
  --mbn<- mapM (return.snd) $ DM.toList $ mn^.bnodes
  
  foldl (perPosessingConnectionToOtherBrach)
             (return S.empty)
             $ map (\x->map mbnode $ snd x) $ DM.toList $ mn^.bnodes
  
  where
  perPosessingConnectionToOtherBrach :: (
                --  PosessingConnectionToOtherBrach n, 
                  MonadState MeaningfullNodes m 
                 ,MonadReader HticConfig m 
                 ,MonadWriter (DList ApplicationLog) m 
                )
                => m (S.Set ConnectionOverSomething)
                -> [m BNode]
                -> m (S.Set ConnectionOverSomething)
  perPosessingConnectionToOtherBrach ms n = do
    mn<-get
    s<-ms
    n'<- sequence n
    noteV "all" $ show n'
    mapM_ (\c->noteV "all" $ ((++) "s-" .show) c) s
    mapM_ (\c->noteV "all" $ ((++) "c-" .show) c) 
      $ concatMap (\x-> getConnections x) n'
    
    
    let s' = S.union s 
               $ S.fromList $ concat [
                 concatMap (\x-> getConnections x) n'
               {- ,concatMap (\x-> maybe [] 
                                      (concat.(map getConnections)
                                       .(\x->mapMaybe 
                                              toTNodes 
                                              $ selectiveGet x mn)
                                       .toMeaningfullReference
                                       ) 
                                      $ tNodeReference x) n'-}
                ]
               
    mapM_ (\c->noteV "all" $ ((++) "s'-" .show) c) s'
    return s'

    where
    toTNodes (IsTNode n) = Just n
    toTNodes _ = Nothing


  mbnode :: MonadState MeaningfullNodes m 
                   => BNode -> m BNode
  mbnode n = return n




isConnectionToTextFillIn tfinids cn = (elem (cn^.cn_start) tfinids) 
                                      || (elem (cn^.cn_end) tfinids)

isConnectionToChosenNode csnids cn = (elem (cn^.cn_start) csnids) 
                                      || (elem (cn^.cn_end) csnids)
                                      
excludeConnectionsToTextFillIns :: [Connection] 
                                -> [T.Text]
                                -> [Connection] 
excludeConnectionsToTextFillIns cns tfinids = filter 
  (not . isConnectionToTextFillIn tfinids) 
  cns


excludeConnectionsToChosenNodes :: [Connection] 
                                -> [T.Text]
                                -> [Connection] 
excludeConnectionsToChosenNodes cns csnids = filter 
  (not . isConnectionToChosenNode csnids) 
  cns
  

  
isolateConnectionsToTextFillIns :: [Connection] 
                                -> [T.Text]
                                -> [Connection] 
isolateConnectionsToTextFillIns cns tfinids = filter 
  (isConnectionToTextFillIn tfinids) 
  cns


isolateConnectionsToChosenNodes :: [Connection] 
                                -> [T.Text]
                                -> [Connection] 
isolateConnectionsToChosenNodes cns csnids = filter 
  (isConnectionToChosenNode csnids) 
  cns


ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndingsM 
                                    :: MonadState MeaningfullNodes m 
                                    => m ()
ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndingsM = do 
  mn<-get
  let connectionsToTextFillIns = isolateConnectionsToTextFillIns
                                   (mn^.connections) 
                                   (DM.keys $ mn^.textFillIn)
  let connectionsToChosenNodes = isolateConnectionsToChosenNodes
                                   (mn^.connections) 
                                   (DM.keys $ mn^.chosen)
  let (g,c) = ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndings 
                ((\x->excludeConnectionsToChosenNodes
                      x (DM.keys $ mn^.chosen))
                  $ excludeConnectionsToTextFillIns 
                      (mn^.connections) (DM.keys $ mn^.textFillIn)
                ) 
                (concatMap snd $ DM.toList $ mn^.goto)
  connections .=  (union connectionsToChosenNodes 
                    $ union connectionsToTextFillIns
                            c
                  )
  goto        .= (DM.fromListWith (++) $ map (\x-> (getId x,[x])) g)




ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndingsM' 
                                     :: MonadState MeaningfullNodes m 
                                     => [Meaningfull] -> m [Meaningfull] 
ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndingsM' m = do 
  mn<-get
  let connectionsToTextFillIns = isolateConnectionsToTextFillIns
                                   (mn^.connections) 
                                   (DM.keys $ mn^.textFillIn)
  let connectionsToChosenNodes = isolateConnectionsToChosenNodes
                                   (mn^.connections) 
                                   (DM.keys $ mn^.chosen)
  
  
  let (gotos,rest) = partition isGoto m
  let (g,c) = ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndings 
                ((\x->excludeConnectionsToChosenNodes
                      x (DM.keys $ mn^.chosen))
                  $ excludeConnectionsToTextFillIns 
                      (mn^.connections) (DM.keys $ mn^.textFillIn)
                )  
                $ map (\(IsGoto x)-> x) gotos
  connections .=  (union connectionsToChosenNodes 
                    $ union connectionsToTextFillIns
                            c
                  )
  goto        .= (DM.fromListWith (++) $ map (\x-> (getId x,[x])) g)
  return $ concat [map (IsGoto) g,rest]
  
  where
  isGoto (IsGoto _) = True
  isGoto _          = False






ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndings 
                                   :: [Connection] 
                                   -> [Goto] 
                                   -> ([Goto],[Connection])
ifGotosMarkedTheSameAttouchBeginningsToTargetAndDiscardEndings cns gts = 
  (\(g,c)-> (concat g,c)) 
    $ runState (alterConnections (groupBy sameMark 
                                          $ sortOn (\x->x^.g_mark) gts
                                 )
               ) 
               cns
  where
  sameMark g1 g2 
    |(isJust $ g1^.g_mark) && (isJust $ g2^.g_mark) = 
                                            (g1^.g_mark) == (g2^.g_mark)
    |otherwise = False
  
  
  alterMarkedConnections :: [Goto] -> State [Connection] [Goto]
  alterMarkedConnections [] = return []
  alterMarkedConnections gts = do 
    cns<-get
    
    let beginnings = select (isBeginning) gts cns
    let endings = select (isEnding) gts cns
    
    whenEndingIsSingleConnectBeginningsToTargetNode beginnings 
                                                    endings
    
  alterConnections :: [[Goto]] -> State [Connection] [[Goto]]
  alterConnections [] = return []
  alterConnections (gts:rest) = 
    case ((all (\gn-> isJust $ gn^.g_mark) gts)) of
      False -> do 
                  rest'<-alterConnections rest
                  return ([gts] ++ rest')
      True  -> do
                  gts' <- alterMarkedConnections gts
                  rest'<-alterConnections rest
                  return ([gts'] ++ rest')
    
  
  
  whenEndingIsSingleConnectBeginningsToTargetNode :: [(Goto,[Connection])]
                                                  -> [(Goto,[Connection])]
                                                  -> State [Connection] [Goto]
  whenEndingIsSingleConnectBeginningsToTargetNode 
    beginnings 
    [(eg,ecns)] = do 
      mapM 
        (\(bg,_)-> do
          cns<-get
          let dcn = map (\ecn-> set cn_start (bg^.g_id) ecn) ecns
          let cns' = cns \\ ecns
          put (dcn ++ cns')
          return $ set g_mark Nothing bg
        ) 
        beginnings

  whenEndingIsSingleConnectBeginningsToTargetNode 
    beginnings 
    [] = return $ map fst beginnings
    
  whenEndingIsSingleConnectBeginningsToTargetNode 
    beginnings 
    endings = do 
      many_gts_lists <- mapM (\e-> whenEndingIsSingleConnectBeginningsToTargetNode 
                                      beginnings 
                                      [e]
                             )
                             endings
      
      return $ head many_gts_lists
      
  select this gts cns = filter (\(gn,cns)-> not $ null cns)
                          $ map (\(gn,cns)-> (gn,filter (this gn) cns))
                                $ zip gts 
                                      $ repeat cns

  isBeginning goto c = c^.cn_end   == (goto^.g_id) 
  isEnding    goto c = c^.cn_start == (goto^.g_id) 








