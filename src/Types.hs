{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

{-#  LANGUAGE FlexibleInstances #-}
{-#  LANGUAGE FlexibleContexts #-}
{-#  LANGUAGE TypeSynonymInstances #-}


module Types (
 Template(..)
,ConnectionOverSomething(..)
,TextFillInReference(..)
,GotoReference(..)
,BNodeReference(..)
,TNodeReference(..)
,Branch(..)
,BNode(..)
,TNode(..)
,Connection(..)
,DirectionAware(..)
,DisplayOnNewPageNode(..)
,DisplayOnNewPageNodeReference(..)
,Point(..)
,TextFillIn(..)
,Goto(..)
,Meaningfull(..)
-- ,MeaningfullReferences(..)
,MeaningfullNodes(..)
,ApplicationLog(..)
,HticConfig(..)
,NodeStructure(..)
,AltContent(..)
,AltContentReference(..)
,MeaningfullReferences(..)
,RandomNext(..)
,RandomNextReference(..)
,AutoContinue(..)
,AutoContinueReference(..)
,OverrideNext(..)
,OverrideNextReference(..)
,RunCode(..)
,RunCodeNodeReference(..)
,Chosen(..)
,ChosenNodeReference(..)
,unwrapMeaningfullBnode
,evalNodeStructure
,runNodeStructure
) where


import Data.Maybe
import Data.List
import qualified Data.Map as DM
import qualified Data.Text as T
import Control.Lens hiding ((|>),(<.>))
import qualified Control.Lens as L --hiding (element)
import Control.Monad.State.Lazy
import qualified Control.Monad.State.Lazy as St
import Control.Monad.RWS.Lazy
import qualified Control.Monad.RWS.Lazy as Rws
import GHC.Generics (Generic, Generic1)

import Data.DList hiding (tail,map,empty,fromList,head,concat,lookup,foldr
                         ,replicate)



data Template = 
  TplData T.Text
  |TplCache T.Text
  deriving (Show)


data ConnectionOverSomething = 
   ConnectionOverGoto GotoReference BNodeReference
   |DirectConnection BNodeReference
   |HangingConnection GotoReference
   deriving (Show)


data TextFillInReference = TxR T.Text deriving (Eq,Ord,Show)
data GotoReference       = GR  T.Text deriving (Eq,Ord,Show)
data BNodeReference      = BNR T.Text deriving (Eq,Ord,Show)
data TNodeReference      = TNR T.Text deriving (Eq,Ord,Show)
data DisplayOnNewPageNodeReference = DONPR T.Text deriving (Eq,Ord,Show)
data AltContentReference = ACR T.Text deriving (Eq,Ord,Show)
data RandomNextReference = RNR T.Text deriving (Eq,Ord,Show)
data AutoContinueReference = AUCR T.Text deriving (Eq,Ord,Show)
data OverrideNextReference = OVNR T.Text deriving (Eq,Ord,Show)
data RunCodeNodeReference = RNCR T.Text deriving (Eq,Ord,Show)
data ChosenNodeReference = CsnR T.Text deriving (Eq,Ord,Show)

data Branch = B
  {_b_name::T.Text
  ,_b_option::T.Text
  ,_b_optionChosenState::Maybe T.Text
  ,_b_content::T.Text
  ,_b_next::[T.Text]
  ,_b_displayOnNewPage::Bool
  ,_b_altContent::Maybe T.Text
  ,_b_altContentTimeout::Maybe Word
  ,_b_autoContinue::Maybe Word
  ,_b_randomNext::Bool
  ,_b_buildFromTamplate::[Template]
  ,_b_overrideNext::Maybe T.Text
  ,_b_skipPastNext::Maybe T.Text
  ,_b_runCode::Maybe T.Text
  }deriving (Show)
L.makeLenses ''Branch

data BNode = BN
  {_bn_name::T.Text
  ,_bn_textRelated::[TextFillInReference]
  ,_bn_branchRelated::[ConnectionOverSomething]
  ,_bn_content::T.Text 
  ,_bn_displayOnNewPage::Bool
  ,_bn_altContent::Maybe AltContentReference
  ,_bn_randomNext::Bool
  ,_bn_autoContinue::Maybe AutoContinueReference
  ,_bn_overrideNext::Maybe T.Text
  ,_bn_skipPastNext::Maybe T.Text
  ,_bn_template::Maybe TNodeReference
  ,_bn_runCode::Maybe RunCodeNodeReference
  }deriving (Show)
L.makeLenses ''BNode

data AltContent = AC
  {_ac_id::T.Text
  ,_ac_textRelated::[TextFillInReference]
  ,_ac_content::T.Text 
  ,_ac_timeout::Maybe Word
  }deriving (Show)
L.makeLenses ''AltContent

data AutoContinue = AUC
  {_auc_id::T.Text
  ,_auc_timeout::Maybe Word
  }deriving (Show)
L.makeLenses ''AutoContinue

data OverrideNext = OVN
  {_ovn_id::T.Text
  ,_ovn_override::T.Text
  ,_ovn_skips   ::Maybe T.Text
  }deriving (Show)
L.makeLenses ''OverrideNext


data Connection = Cn 
  {_cn_start::T.Text
  ,_cn_end  ::T.Text
  
  }deriving (Show,Eq)
L.makeLenses ''Connection



data DirectionAware a = ToA a|FromA a deriving (Show)

data DisplayOnNewPageNode = DisplayOnNewPageNode 
  {_donp_id::T.Text} deriving (Show)
L.makeLenses ''DisplayOnNewPageNode
  
data RandomNext = RandomNext 
  {_rndnxt_id::T.Text} deriving (Show)
L.makeLenses ''RandomNext
  
   
data Point = Point 
  {_pt_x :: Double
  ,_pt_y :: Double
  }deriving (Show)
L.makeLenses ''Point



data TextFillIn = Tx
  {_tx_content::T.Text
  ,_tx_name   ::T.Text
  ,_tx_id     ::T.Text
  ,_tx_link   ::Maybe T.Text
  ,_tx_position :: Maybe Point
  }deriving (Show)
L.makeLenses ''TextFillIn



data Goto = G 
  {_g_option ::T.Text
  ,_g_related::[TextFillInReference]
  ,_g_chosen ::Maybe ChosenNodeReference
  ,_g_id     ::T.Text
  ,_g_mark   ::Maybe T.Text
  ,_g_position :: Maybe Point
  }deriving (Show)
L.makeLenses ''Goto

data TNode = TN
  {_tn_id::T.Text
  ,_tn_branchRelated::[ConnectionOverSomething]
  }deriving (Show)
L.makeLenses ''TNode

data RunCode = RnC
  {_rnc_id::T.Text
  ,_rnc_content::T.Text
  ,_rnc_replaceWithBID :: Maybe T.Text
  }deriving (Show)
L.makeLenses ''RunCode


data Chosen = Csn 
  {_csn_option ::T.Text
  ,_csn_related::[TextFillInReference]
  ,_csn_id     ::T.Text
  }deriving (Show)
L.makeLenses ''Chosen



data Meaningfull = 
  IsBNode BNode
  |IsTextFillIn TextFillIn
  |IsGoto Goto
  |IsDisplayOnNewPageNode DisplayOnNewPageNode
  |IsAltContentNode AltContent
  |IsRandomNext RandomNext
  |IsAutoContinue AutoContinue 
  |IsOverrideNext OverrideNext
  |IsTNode TNode
  |IsRunCodeNode RunCode
  |IsChosenNode Chosen
  deriving (Show)
  
data MeaningfullReferences = 
  IsBNodeReference BNodeReference
  |IsTextFillInReference TextFillInReference
  |IsGotoReference GotoReference
  |IsDisplayOnNewPageNodeReference DisplayOnNewPageNodeReference
  |IsAltContentNodeReference AltContentReference
  |IsRandomNextReference RandomNextReference
  |IsAutoContinueReference AutoContinueReference
  |IsOverrideNextReference OverrideNextReference
  |IsTNodeReference TNodeReference
  |IsRunCodeReference RunCodeNodeReference
  |IsChosenReference ChosenNodeReference
  deriving (Show)


data MeaningfullNodes = MeaningfullNodes
    {_bnodes :: DM.Map T.Text [BNode]
    ,_tnodes :: DM.Map T.Text [TNode]
    ,_goto   :: DM.Map T.Text [Goto]
    ,_textFillIn  :: DM.Map T.Text [TextFillIn]
    ,_propDisplayOnNewPage  :: DM.Map T.Text [DisplayOnNewPageNode]
    ,_altContent  :: DM.Map T.Text [AltContent]
    ,_randomNext  :: DM.Map T.Text [RandomNext]
    ,_autoContinue:: DM.Map T.Text [AutoContinue]
    ,_overrideNext:: DM.Map T.Text [OverrideNext]
    ,_runCode     :: DM.Map T.Text [RunCode]
    ,_chosen      :: DM.Map T.Text [Chosen]
    ,_connections :: [Connection]
    }deriving (Generic,Show)
L.makeLenses ''MeaningfullNodes


data ApplicationLog =
    OtherError       String
   |Note             String
   |Dump             String
   |ApplicationLogEmpty
   deriving (Show)






data HticConfig = HticConfig
  {_hticConf_inputFile :: String
  ,_hticConf_outputFile :: String
  ,_hticConf_templateFile :: String
  ,_hticConf_verbosity :: [String]
  }
L.makeLenses ''HticConfig

--newtype NodeStructureState s a = NSS (StateT s Identity a)
--newtype NodeStructure a = NodeStructure (StateT MeaningfullNodes Identity a)
--  deriving (Functor,Applicative,Monad,MonadState MeaningfullNodes)
--type NodeStructure a = StateT MeaningfullNodes Identity a
newtype NodeStructure a = NodeStructure (RWST HticConfig 
                            (DList ApplicationLog) 
                            MeaningfullNodes Identity a)
  deriving (Functor
           ,Applicative
           ,Monad
           
           ,MonadState MeaningfullNodes
           ,MonadWriter (DList ApplicationLog)
           ,MonadReader HticConfig
           )


evalNodeStructure (NodeStructure rwst) r s = evalRWST rwst r s
runNodeStructure (NodeStructure rwst) r s = runRWST rwst r s

{-
data NodeStructureState s a = NSS {
  runNSSa :: StateT s Identity a
  }

runNSS :: NodeStructure a 
         -> s 
         -> (a, MeaningfullNodes)
runNSS a s = runState (runNSSa s a) s



data NodeStructure a = NodeStructure {
  runNSa :: StateT MeaningfullNodes Identity a
  }deriving (MonadState MeaningfullNodes)

runNS :: NodeStructure a 
         -> MeaningfullNodes 
         -> (a, MeaningfullNodes)
runNS a mn = runState (runNSa a) mn


instance  Functor NodeStructure where
  fmap :: (a -> b) -> NodeStructure a -> NodeStructure b
  fmap f a = do
    a'<-a
    return $ f a'


instance MonadState s (NodeStructure) where
  get = lift get
  put = lift . put
  state = lift . state
-}
 
 


unwrapMeaningfullBnode :: Meaningfull -> Maybe BNode
unwrapMeaningfullBnode (IsBNode n) = Just n
unwrapMeaningfullBnode _ = Nothing

unwrapMeaningfullTnode :: Meaningfull -> Maybe TNode
unwrapMeaningfullTnode (IsTNode n) = Just n
unwrapMeaningfullTnode _ = Nothing










