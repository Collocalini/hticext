
module Classes (
 OneOfMeaningfull
,PosessingContent
,ContainingTextFillins
,ContainingTextFillinReferences
,TemplateApplicable
,PosessingPropertyDisplayOnNewPage
,TextFillInLike
,AcceptingTextRelated
,PosessingTextRelated
,PosessingTextRelatedMa
,PosessingConnectionToOtherBrach
,CanBuildConnectionToOtherBrach
,HasId
,ReferenceToMeaningfull
,MeaningfullNodesContainer
,ContainingAltContentReference
,ContainingRunCodeReference
,ContainingTNodeReference
,ContainingChosenReference
,PosessingAltContentMa
,PosessingChosenMa
,PosessingRunCodeMa
,AwareOfConnected
,PosessingPropertyRandomNext
,ContainingAutoContinueReference
,PosessingAutoContinueMa
,PosessingTNodeMa
,PosessingPropertyOverrideNext
,PosessingPropertySkipPastNext
,getSkipPastNext
,setSkipPastNext
,getOverrideNext
,setOverrideNext
,getAutoContinue
,setAutoContinue
,setAutoContinue'
,autoContinueReference
,setAutoContinueReference
,ifRandomNext
,setRandomNext
,setRandomNext'
,consumesConnected
,getAnyMeaningfullNode
,toMeaningfullReference
,fromMeaningfullReference
,getId
,refId
,getContent
,putContent
,getConnections
,putConnection
,addConnection
,putConnections
,getTextRelated
,getTextRelatedMa
,putTextRelated
,ifDisplayOnNewPage
,setDisplayOnNewPage
,setDisplayOnNewPage'
,applyTemplate
,applyTemplateT
,toMeaningfull
,fromMeaningfull
,textByReference
,textReferences
,consTextReference
,asTextFillIn
,selectivePut
,selectiveGet
,getAltContent
,setAltContent
,setAltContent'
,getRunCode
,setRunCode
,setRunCode'
,getChosen
,setChosen
,setChosen'
,altContentReference
,setAltContentReference
,runCodeReference
,setRunCodeReference
,tNodeReference
,setTNodeReference
,chosenReference
,setChosenReference
,getTNode
,setTNode
,setTNode'
) where




import qualified Data.Text as T

import Types





class AwareOfConnected a where
  consumesConnected :: DirectionAware Meaningfull -> a -> a



class ReferenceToMeaningfull a where
  refId :: a-> T.Text
  toMeaningfullReference :: a -> MeaningfullReferences
  fromMeaningfullReference :: MeaningfullReferences -> Maybe a


class MeaningfullNodesContainer a where
  selectivePut :: Meaningfull -> a -> a
  selectiveGet :: MeaningfullReferences -> a -> [Meaningfull]
  getAnyMeaningfullNode :: T.Text -> a -> [Meaningfull]


class OneOfMeaningfull a where
  toMeaningfull :: a -> Meaningfull
  fromMeaningfull :: Meaningfull -> Maybe a



class PosessingContent pc where
  getContent :: pc -> T.Text
  putContent :: T.Text -> pc -> pc
  
class HasId a where
  getId :: a -> T.Text
  


class ContainingTextFillins m where
  textByReference :: m T.Text -> m (Maybe TextFillIn)


class ContainingTextFillinReferences a where
  textReferences :: a -> [TextFillInReference]
  consTextReference :: TextFillInReference -> a -> a

class ContainingAltContentReference a where
  altContentReference :: a -> Maybe AltContentReference
  setAltContentReference :: AltContentReference -> a -> a

class ContainingRunCodeReference a where
  runCodeReference :: a -> Maybe RunCodeNodeReference
  setRunCodeReference :: RunCodeNodeReference -> a -> a

class ContainingAutoContinueReference a where
  autoContinueReference :: a -> Maybe AutoContinueReference
  setAutoContinueReference :: AutoContinueReference -> a -> a

class ContainingTNodeReference a where
  tNodeReference :: a -> Maybe TNodeReference
  setTNodeReference :: TNodeReference -> a -> a

class ContainingChosenReference a where
  chosenReference :: a -> Maybe ChosenNodeReference
  setChosenReference :: ChosenNodeReference -> a -> a

class TemplateApplicable m where
  applyTemplate  :: (PosessingContent a, OneOfMeaningfull a) 
                 => m a -> m a
  applyTemplateT :: (PosessingContent a, OneOfMeaningfull a) 
                 => m a -> m T.Text


class PosessingPropertyDisplayOnNewPage pc where
  ifDisplayOnNewPage :: pc -> Bool
  setDisplayOnNewPage:: Meaningfull -> pc -> pc
  setDisplayOnNewPage':: Bool -> pc -> pc
  
class PosessingPropertyRandomNext pc where
  ifRandomNext :: pc -> Bool
  setRandomNext:: Meaningfull -> pc -> pc
  setRandomNext':: Bool -> pc -> pc

class PosessingPropertyOverrideNext pc where
  getOverrideNext :: pc -> Maybe T.Text
  setOverrideNext :: Meaningfull -> pc -> pc

class PosessingPropertySkipPastNext pc where
  getSkipPastNext :: pc -> Maybe T.Text
  setSkipPastNext :: Meaningfull -> pc -> pc

class PosessingAltContentMa m where
  getAltContent :: ContainingAltContentReference a 
                => m a -> m [AltContent]
  setAltContent :: ContainingAltContentReference a 
                => Meaningfull -> m a -> m a
  setAltContent':: ContainingAltContentReference a 
                => AltContent -> m a-> m a

class PosessingRunCodeMa m where
  getRunCode :: ContainingRunCodeReference a 
                => m a -> m [RunCode]
  setRunCode :: ContainingRunCodeReference a 
                => Meaningfull -> m a -> m a
  setRunCode':: ContainingRunCodeReference a 
                => RunCode -> m a-> m a

class PosessingChosenMa m where
  getChosen :: ContainingChosenReference a 
                => m a -> m [Chosen]
  setChosen :: ContainingChosenReference a 
                => Meaningfull -> m a -> m a
  setChosen':: ContainingChosenReference a 
                => Chosen -> m a-> m a


class PosessingAutoContinueMa m where
  getAutoContinue :: ContainingAutoContinueReference a 
                => m a -> m [AutoContinue]
  setAutoContinue :: ContainingAutoContinueReference a 
                => Meaningfull -> m a -> m a
  setAutoContinue':: ContainingAutoContinueReference a 
                => AutoContinue -> m a-> m a

class PosessingTNodeMa m where
  getTNode :: ContainingTNodeReference a 
                => m a -> m [TNode]
  setTNode :: ContainingTNodeReference a 
                => Meaningfull -> m a -> m a
  setTNode':: ContainingTNodeReference a 
                => TNode -> m a-> m a


class TextFillInLike a where
  asTextFillIn :: a -> Maybe TextFillIn

 
class AcceptingTextRelated a where
  putTextRelated :: TextFillInLike t => t -> a -> a


class PosessingTextRelated a where
  getTextRelated :: a -> [TextFillIn]

class PosessingTextRelatedMa m where
  getTextRelatedMa :: (ContainingTextFillinReferences a) 
                   => m a  -> m [TextFillIn]
  
        
class CanBuildConnectionToOtherBrach a where
  addConnection :: Meaningfull -> a -> a
          
                      
class PosessingConnectionToOtherBrach ptr where
  getConnections :: ptr -> [ConnectionOverSomething]
  putConnection :: ptr -> ConnectionOverSomething -> ptr 
  putConnections :: ptr -> [ConnectionOverSomething] -> ptr  
  


  
  
